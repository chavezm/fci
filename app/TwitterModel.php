<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twitter;
use Carbon\Carbon;
class TwitterModel extends Model
{
    
    protected $table = 'twitter_history';

    public static function guardarEnBase(){

        $data = TwitterModel::whereDate('created_at',  Carbon::today())->first();

       // if($data == null){
            $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
            $palabras =  explode(',', $palabras);
            $arr = TwitterModel::getEstadisticasTweetsFiltrados($palabras, null);
            
            $data = new TwitterModel();
            $data->data = json_encode($arr);
            $data->save();
        //}        
    }

    public static function getDesdeBaseFechas($desde, $hasta){

        $data = TwitterModel::whereBetween('created_at',  [$desde, $hasta])->get();
        
        $arr = [];
        $arr['cantPositivos'] = 0;
        $arr['cantNegativos'] = 0;
        $arr['cantNeutrales'] = 0;
        
        foreach($data as $d) {
            $json = json_decode($d->data, true);
            //dd($json);
            foreach($json as $key => $element){
                //dd($element);
                if($key == 'cantPositivos') 
                    $arr['cantPositivos'] += $json['cantPositivos'];
                else if($key == 'cantNegativos')
                    $arr['cantNegativos'] += $json['cantNegativos'];
                else if($key == 'cantNeutrales')
                    $arr['cantNeutrales'] += $json['cantNeutrales'];
                else {
                    if( !isset($arr[$key]) ){
                        $arr[$key] = $element;
                    }else{
                        $arr[$key]['cantPositivos'] += $json[$key]['cantPositivos'];
                        $arr[$key]['cantNegativos'] += $json[$key]['cantNegativos'];
                        $arr[$key]['cantNeutrales'] += $json[$key]['cantNeutrales'];    
                        unset( $json[$key]['cantPositivos']);
                        unset( $json[$key]['cantNegativos']);
                        unset( $json[$key]['cantNeutrales']);
                        $arr[$key] = array_merge($arr[$key],$json[$key]);
                        //dd($json);
                    }
                }
            }
        }  
        
        return $arr;
    }


    public static function getDesdeBase(){
        $data = TwitterModel::whereDate('created_at',  Carbon::today())->first();

        if($data == null)
        {
            TwitterModel::guardarEnBase();
            $data = TwitterModel::whereDate('created_at',  Carbon::today())->first();
        }

       return json_decode($data->data, true);
    }


    public static function busqueda($var, $geo){
        if($geo == null)
            $geocode = Configuracion::where('nombre', 'TWITTER_GEOCODE')->first()->valor;
        else {
            $geocode = $geo;
        }    
        //dd($geocode);
        $array = [  
                    'q' => $var, 
                    'geocode' => $geocode,
                    'format' => 'array',
                    'result_type'=>'recent', 
                    'count' => 99
                 ];
        
        $followers = Twitter::getSearch($array);

        $seguidores=($followers['statuses']);

        return $seguidores;
    }

    public static function filtrarRT($datos){
        
        $final = [];
        
        for($i=0;$i<count($datos);$i++){
            //Filtro de RT
            if( strpos($datos[$i]['text'], 'RT')  === false )
                array_push($final, $datos[$i]);
        }
            
                
        return $final;
    }

    public static function getEstadisticasTweetsFiltrados($palabras, $geo){
        
        
        $sentiment = new \PHPInsight\Sentiment();

        $tweets = [];
        $arrPalabras = [];

        for($i=0;$i<count($palabras);$i++){            
            $busqueda = TwitterModel::busqueda($palabras[$i],$geo);
            $filtro = TwitterModel::filtrarRT($busqueda);
            $tweets += $filtro;

            $arrPalabras[$palabras[$i]]['data'] = $filtro;
        }      
        
        $arr = [];
        $arr['cantPositivos'] = 0;
        $arr['cantNegativos'] = 0;
        $arr['cantNeutrales'] = 0;
        foreach($palabras as $palabra){
            $arr[$palabra] = [];
            $arr[$palabra]['cantPositivos'] = 0;
            $arr[$palabra]['cantNegativos'] = 0;
            $arr[$palabra]['cantNeutrales'] = 0;
            
            foreach($arrPalabras[$palabra]['data'] as $follow){
                //dd($follow);
                $sentimiento['data'] = array( 
                                    'user_data' => $follow['user'],
                                    'text' => $follow['text'], 
                                    'score' => $sentiment->score($follow['text']), 
                                    'class' => $sentiment->categorise($follow['text']),
                                    'geo' => $follow['geo']
                );
    
                switch($sentimiento['data']['class']) {
                    case 'positivo':
                        $arr[$palabra]['cantPositivos'] ++;
                        $arr['cantPositivos'] ++;
                    break;
                    case 'negativo';
                        $arr[$palabra]['cantNegativos'] ++;
                        $arr['cantNegativos'] ++;
                    break;
                    case 'neutral':
                        $arr[$palabra]['cantNeutrales'] ++;
                        $arr['cantNeutrales'] ++;
                    break;
    
                }
                array_push($arr[$palabra], $sentimiento);
                                          
            }
        }
        return $arr;
    }
}
