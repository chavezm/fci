<?php

namespace App;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class mensaje extends Model
{
    public static function prueba($enviar_numero,$text){
        $sid = 'ACc6283a8cb280d6010084fbb492fdb54c';
        $token = 'd0fc6525c075cacc36a77e835e058cce';
        $client = new Client($sid, $token);
        $fecha = Carbon::now('America/Guayaquil');    
        /*function de mensaje */
        $client->messages->create(
            // the number you'd like to send the message to
            $enviar_numero,
            array(           
                'from' => '+12056276430',
                'body' => $text . $fecha
            )
        );
   
    }

    public static function ws($enviar_numero,$text){
        $sid    = "AC7d8623e38f233d32cee3ca24b62c1a2e";
        $token  = "d01aa34d2b9290e764a6f2be957fb277";
        $twilio = new Client($sid, $token);
        $fecha = Carbon::now('America/Guayaquil');    
        $message = $twilio->messages
            ->create("whatsapp:".$enviar_numero,
                    array(
                        "body" => $text.$fecha,
                        "from" => "whatsapp:+14155238886"
                    )
            );
    }


}
