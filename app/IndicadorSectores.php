<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndicadorSectores extends Model
{
  protected $table1 = 'trayectoria_gye_detalle';
  protected $fillable1 = ['id_trayectoria','velocidad','tiempo','id_sector','fecha'];
  protected $primaryKey1 = 'id';
  public $timestamps1 = true;
}
