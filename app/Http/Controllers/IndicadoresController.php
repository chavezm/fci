<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndicadoresController extends Controller{

	public function Indicadores(){
		return view('layouts.admin.Indicadores.indicadores');
	}

	public function IndicadoresPVAS(Request $request){
		return view('indicadores.PVAS');
	}

	public function IndicadoresVPC(){
		return view('indicadores.VPC');
	}

	public function IndicadoresTwitter(){
		return view('indicadores.Twitter');
	}

	public function IndicadoresFHP(){
		return view('indicadores.FHP');
	}

	public function IndicadoresVPT(){
		return view('indicadores.VPT');
	}
}
//prueba