<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller; 
use App\Twiter; // nombre del modelo de para que ingrese a la base
use PHPInsight; // libreria de algoritmo de sentimiento
use Twitter;    // libreria de Twitter para llamar funciones de recursos de apis


class PruebaController extends Controller
{
    public function prueba()
    {
        $arreglobusquedas=['Tráfico','accidente','choque','colisión','embotellamiento',
        'Accidente de tránsito','#ECU911Reporta','@ATMGuayaquil','Choque'];
        $arreglofinal=[];
        
        for($i=0;$i<count($arreglobusquedas);$i++){            
        $busqueda=$this->busqueda($arreglobusquedas[$i]);
        $arreglofinal=$this->guardar($busqueda,$arreglofinal);
        }
        
        $seguidores = ($arreglofinal);
        $sentiment = new \PHPInsight\Sentiment();
        for ($cont = 0; $cont < count($seguidores); $cont++) {
            $DatosSeguidores = ($seguidores[$cont]);

            /************** texto de tuist **************/
            $cadena_buscada = 'RT';
            $tuist = ($DatosSeguidores['full_text']);
            //print_r($tuist);
            $posicion_coincidencia = strrpos($tuist, $cadena_buscada);

            if ($posicion_coincidencia === false) {
                $tuisttext = ($DatosSeguidores['full_text']); //tuist
                print_r($tuisttext);
                $created_at = ($DatosSeguidores['created_at']);
                /************** Algoritmo de sentimeinto **************/
                $scores = $sentiment->score($tuist);
                $class = $sentiment->categorise($tuist);
                $sentimientoDominante = $class;
                /**Score de sentimeinto % de positivo negativo y neutral **/
                $scorePositivo = $scores['positivo'];
                $scoreNegativo = $scores['negativo'];
                $scoreNeutral = $scores['neutral'];
                /************** fin de Algoritmo de sentimeinto **************/
                /************** geo  **************/
                $geo = ($DatosSeguidores['geo']);
                if ($geo != null) {
                    if (is_array($geo)) {
                        $tipoGeo = $geo['type']; //tipo de punto point poligono etc
                        if (is_array($geo['coordinates'])) {
                            //print_r($geo) ;
                            $geolatitud = $geo['coordinates'][0];
                            $geolongitud = $geo['coordinates'][1];
                            $coordenadageo = $geolatitud . ',' . $geolongitud; //coordenada latitud,longitud
                        }
                    }
                } else {
                    $tipoGeo = 'no aplica';
                    $coordenadageo = 'no aplica';
                }
                /************** fin  geo  **************/
                /**************   coordinates  **************/
                $coordinates = ($DatosSeguidores['coordinates']);
                if ($coordinates != null) {
                    if (is_array($coordinates)) {
                        // print_r($coordinates);
                        $tipocoordenada = ($coordinates['type']);
                        $coorde = ($coordinates['coordinates']);
                        if (is_array($coorde)) {
                            if ($coorde != null) {
                                $coorlongitud = ($coordinates['coordinates'][0]);
                                $coorlatitud = ($coordinates['coordinates'][1]);
                                //$listaCordenada=($coorlatitud.','.$coorlongitud);//coordenada latitud,longitud
                                //print_r($listaCordenada);
                            }
                        }
                    }
                } else {
                    /* $coorlatitud=-2.211341;
                    $coorlongitud=-79.901302; */
                    $coorlatitud = 'no aplica';
                    $coorlongitud = 'no aplica';
                }
                var_dump($coorlatitud);
                var_dump($coorlongitud);

                /************** fin  coordinates  **************/
                /**************   users  **************/
                 echo '<pre>';
                $name = ($DatosSeguidores['user']['name']);
                $screen_name = ($DatosSeguidores['user']['screen_name']);
                $userlocation = ($DatosSeguidores['user']['location']);
                $geo_enabled = ($DatosSeguidores['user']['geo_enabled']);
                $url = ($DatosSeguidores['user']['profile_image_url_https']);
                var_dump($name);
                /************** fin  users  **************/
                /*Guardar en base */
                $usuarios = new Twiter;
                $usuarios->nombre = $name;
                $usuarios->screen_name = $screen_name;
                $usuarios->tuits = $tuisttext;
                $usuarios->sentimiento = $sentimientoDominante;
                $usuarios->scorePositivo = $scorePositivo;
                $usuarios->scoreNegativo = $scoreNegativo;
                $usuarios->scoreNeutral = $scoreNeutral;
                $usuarios->coorlatitud = $coorlatitud;
                $usuarios->coorlongitud = $coorlongitud;
                $usuarios->userlocation = $userlocation;

                $usuarios->tipoGeo = $tipoGeo;
                $usuarios->coordenadageo = $coordenadageo;
                $usuarios->geo_enabled = $geo_enabled;
                $usuarios->url = $url;
                $usuarios->save(); 

            }
        }
        return view('twitter.index');
    }

    public function busqueda($var){
        $array=['q'=>$var,'geocode'=>'-2.2058400,-79.9079500,10mi','format' => 'array','result_type'=>'recent', 'count'=>99,
        "tweet_mode" => "extended"];
        $followers = Twitter::getSearch($array);
        $seguidores=($followers['statuses']);
        return $seguidores;
    }

    public function guardar($datos,$final){

        for($i=0;$i<count($datos);$i++)
        {
        array_push($final, $datos[$i]);
        }
        return $final;
    }

    public function datos(){
        $data = Twiter::all();
        dd($data);
        return view('twitter.index', ['seguidores' => $data]);
    }
}
