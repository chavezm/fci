<?php
namespace App\Http\Controllers;


use App\Sectores;
use App\pointLocation;
use Request;
use DB;

class SectoresController extends Controller {

  /*
  |--------------------------------------------------------------------------
  | Home Controller
  |--------------------------------------------------------------------------
  |
  | This controller renders your application's "dashboard" for users that
  | are authenticated. Of course, you are free to change or remove the
  | controller as you wish. It is just here to get your app started!
  |
  */

  /**
   * Create a new controller instance.
   *
   * @return void
   */

  
  public function Sector()
  {
    $sectorp = Sectores::get();
    return view('mapasectoresg')->with("sectorp", $sectorp);    
    }
  
  
  public function Sectorsel($uidsectores)
  {
    $arreglouidsectores = explode(",",$uidsectores);

        $sectorp=DB::select("select * from ge_sectores where uidsector in (".substr($uidsectores,0,strlen($uidsectores)-1).")");    
   
    return view('layouts.admin.sectores.kmeansSectores')->with("sectorp", $sectorp);
    
  }

//12/01/2019 DOUGLAS NATHA - CLASE QUE ME PERMITE OBTENER LOS PUNTOS QUE ESTAN DENTRO DE UN SECTOR.
     // Checar si el punto se encuentra exactamente en uno de los vértices?

  public function agregar_nuevo_sector()
  {
    $datos=Request::all();    
    $sect = new Sectores;
    $sect->sector = $datos["sector"];
    $sect->uidciudad = $datos["uidciudad"];
    $sect->comentario = $datos["comentario"];
    $sect->coordenadas = $datos["coordenadas"];
    $sect->puntomedio = $datos["puntomedio"];
    $sect->estado = $datos["estado"];
    $resul= $sect->save();
    $sectorp = Sectores::get();
    return  view('layouts.admin.sectores.sectores')->with("sectorp", $sectorp);
  }

  public function eliminar_sector()
  {
    $uidsector=Request::all();
    DB::table('ge_sectores')->where('uidsector',$uidsector)->update(['estado' => "I"]);
        $sectorp = Sectores::get();
    return  view('layouts.admin.sectores.eliminaSectores')->with("sectorp", $sectorp);
  }

  
  public function Sectores()    
  { 
    $sectorp = Sectores::get();
    return  view('layouts.admin.sectores.sectores')->with("sectorp", $sectorp);
  }

  public function eliminaSectores()   
  { 
    $sectorp = Sectores::get();
    return  view('layouts.admin.sectores.eliminaSectores')->with("sectorp", $sectorp);
  }


  public function elegirSectorKmeans()    
  { 
    $sectorp = Sectores::get();
    return  view('layouts.admin.sectores.elegirSectorKmeans')->with("sectorp", $sectorp);
  }


  public function kmeansSectores()    
  { 
     $uidsectores=Request::all();

     $arreglouidsectores = explode(",",$uidsectores); /// $uidsectores['ids'] 
     foreach ($arreglouidsectores as $uidsector){
        $sectorp = Sectores::findOrFail($uidsector);
     }
    $sectorp = Sectores::get();
    //$sectorS=Sectores::where('uidsector',$idsector,'IN')->first();
    return view('layouts.admin.sectores.kmeansSectores')->with("sectorp", $sectorp);
  }


    public function ajax_carga_data(Request $request, $fecha_desde,$hora,$origen)
    {
       $f_desde = $fecha_desde;
       $h_hora = $hora;
        $d_origen=$origen;
        $f_desde_mod=$f_desde.' '.$h_hora;        
 
          if($d_origen=='1')
        {
            $d_origen='W';
        }else{
            $d_origen='A';
        }
        

        $datos = DB::select("select * from trayectoria_gyes tg
        inner join trayectoria_gyes_detalle tgd on tg.id = tgd.id_trayectoria
        where to_char(tgd.fecha, 'yyyy-MM-dd')= ?
        and to_char(tgd.fecha, 'HH24:MI') = ?
        and tg.origen=?
        ", [$f_desde, $h_hora,$d_origen]);   
     
        return response()->json($datos);
    }
   
    public function ajax_r_analisis(Request $request, $usuario, $num_cluster, $sector)
    {        
     
        //`c("Rscript C:/Users/jcheverria/Desktop/Jorge Cheverria/fci/prueba/public/R/analisis_kmeans.R");
       //exec('Rscript C:\xampp\htdocs\FCI\public\R\analisis_kmeans1.R 5 4');
       //exec('C:\\"Program Files"\\R\\R-3.5.1\\bin\\Rscript.exe C:\xampp\htdocs\FCI\public\R\analisis_kmeans1.R 5 3');
      // exec('C:\Program Files\R\R-3.5.1\bin\Rscript.exe C:\xampp\htdocs\FCI\public\R\analisis_kmeans1.R 5 3');
      // /var/www/html/FCI/public/R/analisis_kmeans_sectores.R $usuario $num_cluster $sector"
      $comando="Rscript /var/www/fci/public/R/analisis_kmeans_sectores.R $usuario $num_cluster $sector";
      //$comando="Rscript C:\\xampp\htdocs\FCI\public\R\analisis_kmeans_sectores.R $usuario $num_cluster $sector";
      
   
      
       $test=exec($comando,$out,$ok);//$comando;
      
       if($out!=null){
            if($num_cluster==1)
            {
              
                $lat=str_replace('[1]', '', $out[2]);
                $lon=str_replace('[1]', '', $out[3]);
                $arreglo_latitud=str_split($lat,10);
                $arreglo_longitud=str_split($lon,10);
                $arreglo_general=array_merge($arreglo_latitud,$arreglo_longitud);
      
            }else
            {
                $arreglo_latitud=str_split($out[3],10);
                $arreglo_longitud=str_split($out[5],10);
                $arreglo_general=array_merge($arreglo_latitud,$arreglo_longitud);
                $contador=count($arreglo_latitud);
            }
      }else
      {
          $arreglo_general=null;
      }
       
        $array1=json_encode($arreglo_general);
       return response($array1);
    }

    public function ajax_carga_data_insert()
   {    
      
        $jObject = $_POST['jObject'];
        $data = json_decode($jObject);
        $obj_us = $_POST['obj_us'];
        $data_us = json_decode($obj_us);
        DB::table('trayectoria_gye_hist')->where('usuario', '=', $data_us)->delete();

        $cont_data=count($data);

        foreach($data as $itemData)
        {           
              DB::table('trayectoria_gye_hist')->insert(
                  ['usuario' => $data_us
                  , 'latitud' => $itemData[1] 
                  , 'longitud' => $itemData[2]
                  , 'id_tipo_vehiculo' => $itemData[0]
                  , 'uidsector' => $itemData[3]]
              );
          
        }
         $array1=json_encode($cont_data);
         return response()->json($array1);   
   }   
}