<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Twitter;
use App\tbltwitter;
use App\Zona;
use App\Zona2;
use App\TwitterModel;
use PHPInsight; 
use App\Configuracion;
use Illuminate\Support\Facades\Input;

class TwitterController extends Controller
{
    public function indexTwitter(){
       
        $array=['q'=>'@ATMGuayaquil
        ','format' => 'array','result_type'=>'recent','count'=>'50'];
        $followers = Twitter::getSearch($array);
           
            $seguidores=($followers['statuses']);

        return view('twitter.index', ['seguidores' => $seguidores]); 
    }
   
    public function index() 
    {
        $zonas = Zona::all();
        
        $arr = TwitterModel::getDesdeBase();
        $fechaDesdee='null';
        $fechaHastaa = 'null';
        return view('twitter.ventana1', compact('zonas', 'arr','fechaDesdee','fechaHastaa'));
    }

    public function ventana1Post(Request $request){
        
        $zonas = Zona::all();
        $fechaDesdee = $request->fechaDesde;
        
        $fechaHastaa= $request->fechaHasta;
       

        $arr = TwitterModel::getDesdeBaseFechas($request->fechaDesde, $request->fechaHasta);
        
        return view('twitter.ventana1', compact('zonas', 'arr','fechaDesdee','fechaHastaa'));
    }

    public function ventana2(){

        $zonas = Zona2::where("estado", "A")->get();
        //$zonas = Zona2::all();
        
        $zonaReporte = null;
        $coords = [];
        $pal = [];
        $arr = null;
        if(Input::get('zona') != null && Input::get('palabra') != null){
            $zonaReporte = $zonas->where('uidsector', Input::get('zona'))->first();
            //$c = explode(';', $zonaReporte->coordenadas);
            $jsonArr = json_decode($zonaReporte->coordenadas,true)["geometry"]["coordinates"][0];
            $puntoMedio = json_decode($zonaReporte->puntomedio,true)["geometry"]["coordinates"];
            
            $p = $puntoMedio[1].",".$puntoMedio[0].",0.9km";
            // estaba antes en 0.5km
            foreach($jsonArr as $element) {
                array_push($coords, array( 'lat' => $element[0], 'lng' => $element[1]) );
            }
            //array_push($palabra, Input::get('palabra'));
            $pal = explode(",", Input::get('palabra'));
            //dd($pal);
            $arr = TwitterModel::getEstadisticasTweetsFiltrados($pal,$p);
            //dd($arr);
        }
            
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
        $palabras =  explode(',', $palabras);
        //dd($arr);
        return view('twitter.ventana2', compact('zonas', 'pal', 'zonaReporte', 'palabras', 'arr'));
    }

    public function ventana3(){
        $zonas = Zona::all();
        return view('twitter.ventana3', compact('zonas'));
    }

    public function ventana3Post(Request $request){
        
        $zonas = Zona::all();
  
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
        $palabras =  explode(',', $palabras);
        $geo = $request->lat.",".$request->lng.",".$request->kmRadius."km";
        $arr = TwitterModel::getEstadisticasTweetsFiltrados($palabras, $geo);

        return $arr;
    }

    public function createZona()
    {
        $zonas = Zona::all();
        return view('twitter.agregarZona', compact('zonas'));
    }

    public function storeZona(Request $request) 
    {

        //dd($request->all());
        foreach($request->all() as $obj ){
            //dd($obj);
            $zona = new Zona();
            $zona->coordenadas = "";               
               
            for($j = 0; $j < count($obj['latLngs'][0]); $j++)
                $zona->coordenadas = $zona->coordenadas.$obj['latLngs'][0][$j]['lat'].",".$obj['latLngs'][0][$j]['lng'].";";
            
            $zona->nombre = $obj['nombre'];                   
            $zona->save();   
        }
        

        return response()->json([
            'msg' => 'zona creada' 
        ], 201);
    }
}
