<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehiculoCirculacionController{ // VPC

	private $result;

	public function VehiculosEnCirculacion(Request $request){
		if($request->isMethod('post')){
			$vehiculos = DB::select("select c.sector, c.coordenadas, c.puntomedio, (extract(year from a.fecha)) as año, count(a.id_transporte) as vehiculos_privados from public.trayectoria_gyes a 
				inner join public.trayectoria_gyes_detalle b on a.id = b.id_trayectoria 
				inner join public.ge_sectores c on b.id_sector = c.uidsector
				where c.estado='A' and (a.id_transporte = 1 or a.id_transporte = 5) and (a.fecha between ? and ?) 
				group by c.sector, c.coordenadas,c.puntomedio, año order by c.sector;",[
					$request->input('fecha_inicial'),
					$request->input('fecha_final')
				]);
			if(!empty($vehiculos)){
				$this->result = $this->message(false,$vehiculos,"Transaction Successfull");
			}else{
				$this->result = $this->message(true,[],"Transaction Error");
			}
		}
		return response()->json($this->result ?? []);
	}

	public function IndicadoresDetalle(Request $request,$anio=null){
		$detalle = DB::select("select a.id_transporte, d.sector, d.coordenadas, d.puntomedio, (extract(year from a.fecha)) as año, b.nombre, count(a.id_transporte) as vehiculos_privados 
			from public.trayectoria_gyes a inner join public.tipo_vehiculo b on a.id_transporte = b. id_vehiculo
			inner join public.trayectoria_gyes_detalle c on a.id = c.id_trayectoria 
			inner join public.ge_sectores d on c.id_sector = d.uidsector
			where d.estado = 'A' and (a.id_transporte = 1 OR a.id_transporte = 5)
			and (extract(year from a.fecha)=?) group by a.id_transporte, d.sector, d.coordenadas, d.puntomedio, año, b.nombre order by año;",[
			$anio
		]);
		return response()->json($detalle);
	}

	public function message($error,$data,$message){
		return [
			"existeError" => $error,
			"message" => $message,
			"data"=> $data
		];
	}
		
}
