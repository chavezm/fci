<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmbotellamientoController{

	private $result;

	public function IndicadoresTwitter(Request $request){
		if($request->isMethod('post')){
			$vehiculos= DB::select("select created_at, nombre, tuits, sentimiento, userlocation from public.tbl_twitter where (tuits like '%tráfico%' or tuits like '%trafico%' or tuits like '%embotellamiento%' or tuits like '%ecu911%') and (created_at between ? and ?) order by created_at;",[
					$request->input('fecha_inicial'),
					$request->input('fecha_final')
				]);
			if(!empty($vehiculos)){
				$this->result = $this->message(false,$vehiculos,"Transaction Successfull");
			}else{
				$this->result = $this->message(true,[],"Transaction Error");
			}
		}
		return response()->json($this->result ?? []);
	}

	public function message($error,$data,$message){
		return [
			"existeError" => $error,
			"message" => $message,
			"data"=> $data
		];
	}
		
}
