<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VelocidadPromedioController{

	private $result;

	public function IndicadoresVpt(Request $request){ // VPT
		if($request->isMethod('post')){
			$vehiculos= DB::select("select d.coordenadas, d.puntomedio, (extract(year from a.fecha)) as año, b.id_transporte, c.nombre, d.sector, (sum(a.velocidad)/count(a.velocidad)) as Velocidad 
				from public.trayectoria_gyes_detalle a inner join public.trayectoria_gyes b on a.id_trayectoria=b.id 
				inner join public.tipo_vehiculo c on b.id_transporte=c.id_vehiculo inner join public.ge_sectores d on a.id_sector=d.uidsector
				where d.estado='A' and a.velocidad <= 100 and b.id_transporte=? and (a.fecha between ? and ?)
				group by  d.coordenadas, d.puntomedio, año, b.id_transporte, c.nombre, d.sector order by d.sector;",[
					$request->input('tipo_vehiculo'),
					$request->input('fecha_inicial'),
					$request->input('fecha_final')
				]);
			if(!empty($vehiculos)){
				$this->result = $this->message(false,$vehiculos,"Transaction Successfull");
			}else{
				$this->result = $this->message(true,[],"Transaction Error");
			}
		}
		return response()->json($this->result ?? []);
	}

	public function listarTipoVehiculo(Request $request){
		$tipo_vehiculos = DB::select("SELECT * FROM public.tipo_vehiculo;");
		if(!empty($tipo_vehiculos)){
			$this->result = $this->message(false,$tipo_vehiculos,"Transaction Successfull");
		}else{
			$this->result = $this->message(true,[],"Transaction Error");
		}
		return response()->json($this->result ?? []);
	}

	public function message($error,$data,$message){
		return [
			"existeError" => $error,
			"message" => $message,
			"data"=> $data
		];
	}
		
}
