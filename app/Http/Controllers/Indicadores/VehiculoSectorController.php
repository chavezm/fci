<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehiculoSectorController{ // PVAS

	private $result;

	public function IndicadoresSectores(Request $request){
		if($request->isMethod('post')){
			$sector = DB::select("Select x.coordenadas, x.puntomedio, x.id_sector, x.sector, x.total ,  round(((x.total*100)::numeric/(
				Select sum(x.total) total from (  SELECT b.sector, count( distinct a.id_trayectoria) as  total
				FROM public.trayectoria_gyes_detalle a,
				public.ge_sectores b
				where a.fecha between ? and ? and b.uidsector=a.id_sector and b.estado='A'
				group by b.sector) x) ),2) as porcentaje
				from (  SELECT b.coordenadas, b.puntomedio, a.id_sector, b.sector, count( distinct a.id_trayectoria) as  total
				FROM public.trayectoria_gyes_detalle a,
				public.ge_sectores b
				where a.fecha between ? and ? and b.uidsector=a.id_sector and b.estado='A'
				group by  b.coordenadas, b.puntomedio, a.id_sector, b.sector) x",[
					$request->input('fecha_inicial'),
					$request->input('fecha_final'),
					$request->input('fecha_inicial'),
					$request->input('fecha_final')
				]);
			if(!empty($sector)){
				$this->result = $this->message(false,$sector,"Transaction Successfull");
			}else{
				$this->result = $this->message(true,[],"Transaction Error");
			}
		}
		return response()->json($this->result ?? []);
	}

	public function IndicadoresDetalle(Request $request,$id_sector=null){
		$detalle = DB::select("select x.coordenadas, x.puntomedio, x.nombre,x.conteo,round((x.conteo*100)::numeric/(
			select sum(x.conteo) from (SELECT d.nombre, count(distinct a.id_trayectoria) as conteo
			FROM public.trayectoria_gyes_detalle a,public.trayectoria_gyes b,public.ge_sectores c,public.tipo_vehiculo d
			where a.id_trayectoria = b.id and d.id_vehiculo=b.id_transporte and a.id_sector=? 
			and a.fecha between ? and ? group by d.nombre) x),4) as porcentaje
			from (select c.coordenadas, c.puntomedio, d.nombre as nombre, count(distinct a.id_trayectoria) as conteo 
			FROM public.trayectoria_gyes_detalle a, public.trayectoria_gyes b, public.ge_sectores c, public.tipo_vehiculo d 
			where b.id=a.id_trayectoria and d.id_vehiculo=b.id_transporte and a.id_sector=? 
			and a.fecha between ? and ?
			group by c.coordenadas, c.puntomedio, d.nombre) x",[
					$id_sector,
					$request->input('fecha_inicial'),
					$request->input('fecha_final'),
					$id_sector,
					$request->input('fecha_inicial'),
					$request->input('fecha_final')
			]);
		return response()->json($detalle);
	}

	public function message($error,$data,$message){
		return [
			"existeError" => $error,
			"message" => $message,
			"data"=> $data
		];
	}
		
}
