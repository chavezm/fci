<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HoraPicoController{ // FHP

	private $result;

	public function IndicadoresFhp(Request $request){
		if($request->isMethod('post')){
			$vehiculos= DB::select("select b.coordenadas, b.puntomedio, b.sector, d.nombre, (extract(hour from a.fecha)|| ':' || extract(minute from a.fecha)) as hora, count(d.id_vehiculo) as CANT from  public.trayectoria_gyes_detalle a 
				inner join public.ge_sectores b on a.id_sector = b.uidsector 
				inner join public.trayectoria_gyes c on a.id_trayectoria = c.id 
				inner join public.tipo_vehiculo d on c.id_transporte = d.id_vehiculo 
				where a.id_sector=? group by b.coordenadas, b.puntomedio, b.sector, d.nombre, hora order by hora;",[
					$request->input('sector') ?? 16
				]);
			if(!empty($vehiculos)){
				$this->result = $this->message(false,$vehiculos,"Transaction Successfull");
			}else{
				$this->result = $this->message(true,[],"Transaction Error");
			}
		}
		return response()->json($this->result ?? []);
	}

	public function listarSector(Request $request){
		$sectores = DB::select("SELECT uidsector,sector,estado FROM public.ge_sectores WHERE estado='A';");
		if(!empty($sectores)){
			$this->result = $this->message(false,$sectores,"Transaction Successfull");
		}else{
			$this->result = $this->message(true,[],"Transaction Error");
		}
		return response()->json($this->result ?? []);
	}

	public function message($error,$data,$message){
		return [
			"existeError" => $error,
			"message" => $message,
			"data"=> $data
		];
	}
		
}
