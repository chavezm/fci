<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modulos;

class ArquitecturaController extends Controller

{
    public function indexArq(){

        $modelo = \DB::table('LT_modulos')
            ->join('LT_arq_comp', 'LT_modulos.id_arq_comp', '=', 'LT_arq_comp.id_arq_comp')
            ->join('LT_fases', 'LT_modulos.id_fases', '=', 'LT_fases.id_fases')
            ->join('LT_estados', 'LT_modulos.id_estados', '=', 'LT_estados.id_estados')
            ->select('LT_modulos.id_modulos', 'LT_modulos.nombre','LT_modulos.descripcion','LT_arq_comp.detalle_arq_comp' , 'LT_fases.detalle_fases', 'LT_estados.detalle_estados')
            ->get();

        return view('layouts.admin.Arquitectura.index',['modelo' => $modelo]); 
        
    }

    public function indexFormulario($id){

        $modelo = \DB::table('LT_modulos')
            ->join('LT_arq_comp', 'LT_modulos.id_arq_comp', '=', 'LT_arq_comp.id_arq_comp')
            ->join('LT_fases', 'LT_modulos.id_fases', '=', 'LT_fases.id_fases')
            ->join('LT_estados', 'LT_modulos.id_estados', '=', 'LT_estados.id_estados')
            ->join('LT_relaciones', 'LT_relaciones.id_modulo', '=', 'LT_modulos.id_modulos')
            ->join('LT_instancias', 'LT_relaciones.id_instancias', '=', 'LT_instancias.id_instancias')
            ->join('LT_volumenes', 'LT_relaciones.id_volumenes', '=', 'LT_volumenes.id_volumenes')
            ->select('LT_modulos.id_modulos', 'LT_modulos.nombre','LT_modulos.descripcion', 'LT_modulos.observacion','LT_arq_comp.detalle_arq_comp' , 'LT_arq_comp.url_manual_t' , 'LT_arq_comp.url_manual_u' , 'LT_fases.detalle_fases', 'LT_estados.detalle_estados', 
            'LT_instancias.detalle_instancias', 'LT_instancias.codigoAWS', 'LT_instancias.ip_publica' , 'LT_volumenes.detalle_volumenes' , 'LT_volumenes.codigoAWS_V' , 'LT_volumenes.capacidad')
            ->where('LT_modulos.id_modulos', '=', $id)
            ->get();

        return view('layouts.admin.Arquitectura.Formulario.indexF',['modelo' => $modelo]); 
        
    }

    public function indexArqProb(){

        /*$modelo = \DB::table('LT_modulos')
            ->join('LT_arq_comp', 'LT_modulos.id_arq_comp', '=', 'LT_arq_comp.id_arq_comp')
            ->join('LT_fases', 'LT_modulos.id_fases', '=', 'LT_fases.id_fases')
            ->join('LT_estados', 'LT_modulos.id_estados', '=', 'LT_estados.id_estados')
            ->select('LT_modulos.id_modulos', 'LT_modulos.nombre','LT_modulos.descripcion','LT_arq_comp.detalle_arq_comp' , 'LT_fases.detalle_fases', 'LT_estados.detalle_estados')
            ->get();*/

    return view('layouts.admin.Arquitectura.Problemas.indexP'/*,['modelo' => $modelo]*/); 
        
    }
}