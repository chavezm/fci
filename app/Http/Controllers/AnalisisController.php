<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;
use App\Models\Ad_Usuario_Sistema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use PDF;

class AnalisisController extends Controller
{
    
    public function ajax_pdf()
    {
            $filename="Analisis.pdf";
              

            $jObject = $_GET['kmrlatitud'];
            $kmrlatitud = json_decode($jObject);

            $obj_us = $_GET['kmrlongitud'];
            $kmrlongitud = json_decode($obj_us);
            
            $jObject = $_GET['kmplatitud'];
            $kmplatitud = json_decode($jObject);

            $obj_us = $_GET['kmplongitud'];
            $kmplongitud = json_decode($obj_us);

            $jObject = $_GET['dbslatitud'];
            $dbslatitud = json_decode($jObject);

            $obj_us = $_GET['dbslongitud'];
            $dbslongitud = json_decode($obj_us);

            $jObject = $_GET['hcelatitud'];
            $hcelatitud = json_decode($jObject);

            $obj_us = $_GET['hcelongitud'];
            $hcelongitud = json_decode($obj_us);

            $jObject = $_GET['hcnelatitud'];
            $hcnelatitud = json_decode($jObject);

            $obj_us = $_GET['hcnelongitud'];
            $hcnelongitud = json_decode($obj_us);


               $pdf = PDF::loadView('layouts.admin.analisis.AnalisisPdf',compact('kmrlatitud','kmrlongitud', 'kmplatitud','kmplongitud',
                'dbslatitud','dbslongitud', 'hcelatitud','hcelongitud', 'hcnelatitud','hcnelongitud'));
                return $pdf->download('Analisis.pdf');
    }
   
    
    public function ajax_carga_data(Request $request, $fecha_desde,$hora,$origen)
    {   $f_desde = $fecha_desde;
        $d_origen=$origen;
     
        if($d_origen=='1')
        {
            $d_origen='W';
        }else{
            $d_origen='A';
        }
        
        $datos = DB::select("select * from trayectoria_gyes tg
        inner join trayectoria_gyes_detalle tgd on tg.id = tgd.id_trayectoria
        where to_char(tgd.fecha, 'yyyy-MM-dd')= ?
        and to_char(tgd.fecha, 'HH24:MI') = ?
        and tg.origen=?
        ", [$f_desde,$hora,$d_origen]);
    
        return response()->json($datos);
    }
    
    public function ajax_r_analisis(Request $request, $usuario, $num_cluster)
    {        
     
       $comando="Rscript /var/www/fci/public/R/analisis_kmeans1.R $usuario $num_cluster";

       $test=exec($comando,$out,$ok);//$comando;
     
       if($out!=null){
           
        if($num_cluster==1)
        {
           //var_dump($out);
            // var_dump($out[3]);
                $lat=str_replace('[1]', '', $out[2]);
                $lon=str_replace('[1]', '', $out[3]);
                $arreglo_latitud=str_split($lat,10);
                $arreglo_longitud=str_split($lon,10);
                $arreglo_general=array_merge($arreglo_latitud,$arreglo_longitud);
    
            }else
            {
                $arreglo_latitud=str_split($out[3],10);
                $arreglo_longitud=str_split($out[5],10);
                $arreglo_general=array_merge($arreglo_latitud,$arreglo_longitud);
                $contador=count($arreglo_latitud);
            }
        }else
        {
            $arreglo_general=null;
        }
       
        $array1=json_encode($arreglo_general);
       return response($array1);
    }
    public function ajax_guardar_gye()
    {
        $jObject = $_POST['jObject'];
        $data = json_decode($jObject);

        $jCabecera = $_POST['jCabecera'];
        $dataCabecera = json_decode($jCabecera);


         $f_desde = $dataCabecera[0];
         $tipo_vehiculo = $dataCabecera[1];
         $sector = $dataCabecera[2];
         $velocidad = $dataCabecera[3]; 

         
        switch($velocidad)
            {
            case 1:
                $velocidad = '20';
            break;
            case 2:
                $velocidad = '30';
            break;
            case 3:
                $velocidad = '40';
            break;
            case 4:
                $velocidad = '50';
            break;
            case 5:
                $velocidad = '60';
            break;
            case 6:
                $velocidad = '70';
            break;    
            }
        $f_desde_mod=str_replace("T"," ",$f_desde);
        $f_desde_mod=str_replace("+","",$f_desde);
        $observacion="Ciudad Guayaquil";
        $usuario=$dataCabecera[4];
        //var_dump($resultado);

        $id = DB::table('trayectoria_gyes')->insertGetId(
                ['observacion' => $observacion, 
                'fecha' => $f_desde_mod,
                'id_usuario' => $usuario,
                'estado' =>  'A',
                'origen' => 'W',
                'id_transporte' => $tipo_vehiculo
                ]
            );

        $orden=1;
        $tiempo=0.00;
        foreach($data as $x)
        {
            $fecha_detalle = strtotime ( '+'.$tiempo.' second' , strtotime ( $f_desde_mod ) ) ;
            $fecha_detalle = date ( 'Y-m-j H:i:s' , $fecha_detalle); 
            $id_detalle=DB::table('trayectoria_gyes_detalle')->insertGetId(
                ['id_trayectoria' =>$id, 
                'longitud' => $x[1],
                'latitud' => $x[0],
                'orden' =>  $orden,
                'velocidad' => $velocidad,
                'tiempo' => $tiempo,
                'id_sector' => $sector,
                'fecha'=>  $fecha_detalle
                ]
            );

         
            $orden++;      
            $tiempo=$tiempo+5;
        }
        
        $array1=json_encode($id);
        //$prueba='Completado';
        return response()->json($array1);
    
    }

    
   public function ajax_python(Request $resquest, $usuario, $cluster,$algoritmo){
    if($algoritmo==1){
    $test1 = exec("python3 /var/www/fci/resources/views/layouts/admin/analisis/kmean.py $usuario $cluster");
     $a=array();
    $linea = 0;
    $archivo = fopen("/var/www/fci/resources/views/layouts/admin/analisis/data/Kmean/centroides-KmeanCal.csv", "r");
    while (($datos = fgetcsv($archivo, ",")) == true) 
    {
      $num = count($datos);

      $linea++;
      if($linea>1){

      array_push($a,$datos[1]);
      array_push($a,$datos[2]);
      }

    }
    fclose($archivo);
    
    }
    if($algoritmo==2){
    $test1 = exec("python3 /var/www/fci/resources/views/layouts/admin/analisis/DBSCAN.py $usuario");
    $a=array();
    $linea = 0;
    $archivo = fopen("/var/www/fci/resources/views/layouts/admin/analisis/data/DbScan/centroides-dbscanCal.csv", "r");
    while (($datos = fgetcsv($archivo, ",")) == true) 
    {
      $num = count($datos);

      $linea++;
      if($linea>1){

      array_push($a,$datos[1]);
      array_push($a,$datos[2]);
      }
    }
    
    fclose($archivo);
    }
    if($algoritmo==3){
    $test1 = exec("python3 /var/www/fci/resources/views/layouts/admin/analisis/HCE.py $usuario $cluster");
    $a=array();
    $linea = 0;
    $archivo = fopen("/var/www/fci/resources/views/layouts/admin/analisis/data/HCe/centroides-HCne.csv", "r");
    while (($datos = fgetcsv($archivo, ",")) == true) 
    {
      $num = count($datos);

      $linea++;
      if($linea>1){

     array_push($a,$datos[2]);
      array_push($a,$datos[1]);
      }
    }
    
    fclose($archivo);
    }
    if($algoritmo==4){
    $test1 = exec("python3 /var/www/fci/resources/views/layouts/admin/analisis/HCNE.py $usuario $cluster");
    $a=array();
    $linea = 0;
    $archivo = fopen("/var/www/fci/resources/views/layouts/admin/analisis/data/HCne/centroides-HCne.csv", "r");
    while (($datos = fgetcsv($archivo, ",")) == true) 
    {
      $num = count($datos);

      $linea++;
      if($linea>1){
     array_push($a,$datos[2]);
      array_push($a,$datos[1]);
      }
    }
    
    fclose($archivo);
    }

    return response($a);
}
public function ajax_carga_data_insert()
{    

   $jObject = $_POST['jObject'];
   $data = json_decode($jObject);

   $obj_us = $_POST['obj_us'];
   $data_us = json_decode($obj_us);

   DB::table('trayectoria_gye_hist')->where('usuario', '=', $data_us)->delete();
    
   $cont_data=count($data);

   foreach($data as $itemData)
   {           
        DB::table('trayectoria_gye_hist')->insert(
            ['usuario' => $data_us
            , 'latitud' => $itemData[1] 
            , 'longitud' => $itemData[2]
            , 'id_tipo_vehiculo' => $itemData[0]]
        );
    
     }
   $array1=json_encode($cont_data);
   return response()->json($array1);
}   


}
