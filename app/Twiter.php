<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Twiter extends Model
{
    protected $table = 'tbl_twitter';
    protected $fillable = [
        'nombre','tuits','scorePositivo','scoreNegativo','scoreNeutral','coorlatitud','coorlongitud',
    ];
}
