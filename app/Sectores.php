<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sectores extends Model
{
  protected $table = 'ge_sectores';
  protected $fillable = ['sector','uidciudad','comentario','coordenadas','puntomedio','estado'];
  protected $primaryKey = 'uidsector';
  public $timestamps = true;
}
