<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    protected $table = 'LT_modulos';
    protected $fillable = [
        'id_modulos','nombre','descripcion','id_arq_comp','id_fases','id_estados','costo_mensual','observacion',
    ];
}
