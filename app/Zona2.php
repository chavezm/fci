<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona2 extends Model
{
    protected $table = "ge_sectores";
}
