<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class M2LTArqComp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LT_arq_comp', function (Blueprint $table) {
            $table->increments('id_arq_comp');
            $table->text('detalle_arq_comp');
            $table->text('url_manual_t');
            $table->text('url_manual_u');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LT_arq_comp');
    }
}
