<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class M8LTModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LT_modulos', function (Blueprint $table) {
            $table->increments('id_modulos');
            $table->text('nombre');
            $table->text('descripcion');
            $table->unsignedInteger('id_arq_comp'); 
            //$table->foreign('id_arq_comp')->references('id_arq_comp')->on('LT_arq_comp');
            $table->unsignedInteger('id_fases');
            //$table->foreign('id_fases')->references('id_fases')->on('LT_fases');
            $table->unsignedInteger('id_estados');
            //$table->foreign('id_estados')->references('id_estados')->on('LT_estados');
            $table->timestamp('created_at')->nullable();
            $table->float('costo_mensual', 8, 2);
            $table->text('observacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LT_modulos');
    }
}
