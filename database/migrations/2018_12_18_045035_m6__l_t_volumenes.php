<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class M6LTVolumenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LT_volumenes', function (Blueprint $table) {
            $table->increments('id_volumenes');
            $table->text('detalle_volumenes');
            $table->text('codigoAWS');
            $table->text('capacidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LT_volumenes');
    }
}
