<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Twitters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_twitter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('screen_name');
            $table->longText('tuits');
            $table->string('sentimiento');
            $table->string('scorePositivo');
            $table->string('scoreNegativo');
            $table->string('scoreNeutral');
            $table->string('coorlatitud');
            $table->string('coorlongitud');
            $table->string('userlocation');
            $table->string('tipoGeo');
            $table->string('coordenadageo');
            $table->boolean('geo_enabled');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_twitter');
    }
}
