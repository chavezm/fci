<?php

use Illuminate\Database\Seeder;
use App\Configuracion;

class ConfiguracionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first();
        
        if($palabras == null){

            $configuracion = new Configuracion;

            $configuracion->nombre = 'PALABRAS_TWITTER';
            $configuracion->categoria = 'TWITTER';
            $configuracion->descripcion = 'PALABRAS QUE SE BUSCARAN EN TWITTER';
            $configuracion->valor = 'Guayaquil,Tráfico,accidente,choque,colisión,embotellamiento,Accidente de tránsito,#ECU911Reporta,@ATMGuayaquil';
    
            $configuracion->save();
        }

        $geocode = Configuracion::where('nombre', 'TWITTER_GEOCODE')->first();
        
        if($geocode == null){
            
            $configuracion = new Configuracion;

            $configuracion->nombre = 'TWITTER_GEOCODE';
            $configuracion->categoria = 'TWITTER';
            $configuracion->descripcion = 'GEOCODE TWITTER';
            $configuracion->valor = '-2.2058400,-79.9079500,10mi';
    
            $configuracion->save();
        }
    }
}
