@extends('layouts.admin.base')
@section('title', 'Analisis Guayaquil') 
@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #mapid { height: 400px; width:100%}
</style>

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-3">
                <h1 align="center">Análisis de Sentimiento </h1>
                <h1 align="center">Guayaquil</h1>
            </div>
            <div class="col-lg-offset-1 col-lg-4">
                
                <form action="{{route('ventana1Post') }}" method="POST">
                    <label for="">Fecha Inicio:</label>
                    <input type="date" class="form-control" name="fechaDesde"  required>
                    <label for="">Fecha fin:</label>
                    <input type="date" class="form-control" name="fechaHasta" required>
                    <br>
                    <input type="submit" onclick="myFunction()" class="btn btn-primary " value="Buscar">
                    @if($fechaDesdee != 'null')
                        <p> Fecha de busqueda {{$fechaDesdee}}  hasta {{$fechaHastaa}} </p>
                    @endif
                </form>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="mapid"></div>        
            </div>
        </div>
        <br>
        <div class="row">
         <!--   <a href="{{ route('crearZona') }}" class="btn btn-primary">Crear zona</a> -->
        <br>
        </div>
        
        
        <!--inicio2 -->
        <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>REPORTE DE  ANÁLISIS DE <small>SENTIMIENTO GENERAL</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="dashboard-widget-content">
                            <div >
                                    <canvas id="bar-chart" width="400" height="450"></canvas>
                            </div> 
                    </div>
                  </div>
                </div>
              </div>
        <!-- fin-->       
            <div >
                    
            <!-- inicio -->
            
            <!-- fin-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Visitors location <small>geo-presentation</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="dashboard-widget-content">
                                <div class="row">
                                    <div ">
                                        <div class="panel panel-default">
                                            <!-- Default panel contents -->
                                            <div class="panel-heading">Estados</div>
                                        
                                            <!-- List group -->
                                            <ul class="list-group">
                                                @foreach($arr as $key => $palabra)
                                                    @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')                                                
                                                        <li class="list-group-item">
                                                            {{ $key}}
                                                            <div class="material-switch pull-right">
                                                                <input id="chk{{ str_replace(' ','',$key) }}" value="{{ str_replace(' ','',$key) }}" name="someSwitchOption001"  checked type="checkbox"/>
                                                                <label for="chk{{ str_replace(' ','',$key) }}"  class="label-default"></label>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach                        
                                            </ul>
                                        </div>            
                                    </div>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!--inicio vista de pastel -->
                <div class="">
                    <div class="x_panel tile fixed_height_1000">
                        <div class="x_title">
                            <h2>Análisis</h2>
                            <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 21</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="dashboard-widget-content">                  
                                <div class="row">
                                    <div class="">
                                        <div style="max-height: 500px !important;overflow-x: scroll;">
                                            @foreach($arr as $key => $palabra)
                                                @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
                                                <div id="{{ str_replace(' ','',$key) }}">
                                                    <h5 align="center">REPORTE DE  ANÁLISIS DE</h5>
                                                    <h5 align="center">SENTIMIENTO GENERAL</h5>
                                                    <h4 align="center">{{ $key }}</h4>
                                                    <canvas id="pie-chart{{ $key }}" width="800" height="450"></canvas>
                                                </div>
                                                <hr>       
                                                <br> 
                                                @endif
                                            @endforeach
                                        </div>
                                    </div> 
                                </div>
                            </div>                
                        </div>
                    </div>
                </div>
            <!--fin  vista de pastel -->
          </div>  
        </div>
        </div>
        <br>
        <div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel"> Análisis...</h4>
                    </div>
                    <div class="modal-body"  align="center">
                    <img src="{{ asset('img/images/cargando_ajax.gif') }}" alt="">
                    
                    </div>
                </div>
            </div>
        </div>
   
    <style>
    

    .material-switch > input[type="checkbox"] {
        display: none;   
    }
    
    .material-switch > label {
        cursor: pointer;
        height: 0px;
        position: relative; 
        width: 40px;  
    }
    
    .material-switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position:absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }
    .material-switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }
    .material-switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }
    .material-switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }.material-switch > input[type="checkbox"] {
        display: none;   
    }
    
    .material-switch > label {
        cursor: pointer;
        height: 0px;
        position: relative; 
        width: 40px;  
    }
    
    .material-switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position:absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }
    .material-switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }
    .material-switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }
    .material-switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
    


        </style>
@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
    <script>
         function myFunction() {
            $('#miModal').modal("show");
        }   
        var mymap = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiYXJtYW5kb2NoYXZleiIsImEiOiJjanNvOTN2c3gwa2VwM3ltbGlranRzaTU5In0.Gp0kaVIbVkMcmXZ56zXkzw'
        }).addTo(mymap);


        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: ["Positivo", "Negativo", "Neutral"],
                datasets: [{
                        backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                        data: [
                            {{ $arr['cantPositivos'] }},
                            {{ $arr['cantNegativos'] }},
                            {{ $arr['cantNeutrales'] }}
                        ]                    
                }]
            },
            options: {
                legend: { 
                    display: false 
                    },
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
            }
        });
        
        @foreach($arr as $key => $palabra)
        @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
            
            @foreach($palabra as $p)
                @if($p['data']['geo'] != null)
                    createMarker(mymap, {{ $p['data']['geo']['coordinates'][0] }}, {{ $p['data']['geo']['coordinates'][1] }},"{{ $p['data']['class'] }}","{{ $p['data']['user_data']['profile_image_url_https'] }}","{{ $p['data']['user_data']['name'] }}")
                @endif
            @endforeach

            new Chart(document.getElementById("pie-chart{{ $key }}"), {
                type: 'pie',
                data: {
                    labels: ["Positivo", "negativo", "neutral"],
                    datasets: [{
                        backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                        data: [
                                {{ $palabra['cantPositivos'] }},
                                {{ $palabra['cantNegativos'] }},
                                {{ $palabra['cantNeutrales'] }}
                            ]
                            
                    }]
                },
                options: {
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
                }
            });
            @endif
        @endforeach
       
        function createMarker(map,lt,lg,description,img,nombre){
            if(description== 'negativo'){
                var url ='https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png'
               }else if(description== 'positivo') {
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png';
               }else{
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png';
               }
               
               var greenIcon = new L.Icon({
                   iconUrl: url ,
                   shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                   iconSize: [25, 41],
                   iconAnchor: [12, 41],
                   popupAnchor: [1, -34],
                   shadowSize: [41, 41]
                 }); 

            L.marker([lt, lg],{icon: greenIcon})
            .bindPopup('<table class="table table-condensed"><tbody><tr><td><img src="'+img+'" height="100" width="100"></td><td>'+description+'</td><td>'+nombre+'</td></tr></tbody></table>')
            .addTo(map)
        }
        @foreach($arr as $key => $palabra)
            @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
            $('#chk{{ str_replace(' ','',$key) }}').change(function () {
                console.log(this);
                if (!this.checked) {           
                    $('#{{ str_replace(' ','',$key) }}').hide();              
                }else{            
                    $('#{{ str_replace(' ','',$key) }}').show();             
                }
            });
            @endif
        @endforeach
    </script>

@endsection