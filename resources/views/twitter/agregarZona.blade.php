@extends('layouts.admin.base')
@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin=""/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.css" />
    <style>         
     #mapid { height: 600px; width:100%}

	</style> 
@endsection
@section('content')
    <h1 align="center">Google maps</h1>
    <hr>
    <form action="" method="POST" id="frmZona">
    @csrf
    <div class="container">
        <div class="row">          
            <div class="col-lg-12">
                <div id="mapid"></div>                      
            </div>
            <div class="col-lg-6">
                <input type="text" name="coordenadas" id="coordsZona" value="" style="visibility: hidden;">
                <input type="text" name="nombre"  style="visibility: hidden;">
            </div>
        </div>
    </div>
        <input type="submit" value="Guardar Zonas" class="btn btn-primary" id="btnGuardar">
    </form>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <form id="frmNombre">
        
      <div class="modal-header">
        <h4 align="center" class="modal-title">Datos principales</h4>
      </div>
      <div class="modal-body">
            <label for="">Ingrese el nombre de la Zona:</label>
            <input type="text" name="nombreZona" id="nombreZona" required>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      </form>
      
    </div>

  </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.js"></script>
    <script>
        $('#frmNombre').submit(function (e) {
            e.preventDefault()
            $("#myModal").modal('hide');
        })
        var map = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZGF2ZS1zYWxhemFyIiwiYSI6ImNqcTJtNW5zaDE5cno0M3M3cGY1eWc2NDkifQ.NKsu3DL1RFWfrsn-4BWouQ'
        }).addTo(map);
            
        // Initialise the FeatureGroup to store editable layers
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        // Initialise the draw control and pass it the FeatureGroup of editable layers
        var drawControl = new L.Control.Draw({
            draw: {
                polyline : false,
                rectangle : false,
                circle : false,
                marker: false,
                circlemarker: false,
                polygon: {
                    allowIntersection: false,
                    showArea: true
                }
            },
            edit: {
                featureGroup: drawnItems,
                poly: {
                    allowIntersection: false
                }
            }
        });

        map.addControl(drawControl);

        map.on(L.Draw.Event.CREATED, function (e) {
            var type = e.layerType
            $("#myModal").modal({backdrop: 'static', keyboard: false})
            $("#myModal").on('hide.bs.modal', function () {
                var layer = e.layer.bindTooltip( $('#nombreZona').val(), {permanent: true} );      
                drawnItems.addLayer(layer);   
            });
        });

        $('#frmZona').submit(function (e) {
            e.preventDefault()
            var data = [];
            drawnItems.eachLayer(function(layer) {
                var obj = {
                    latLngs: layer._latlngs,
                    nombre: layer._tooltip._content
                }
                data.push( obj );
            });  
            $.ajax({
                url: '{{ route('guardarZona') }}',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                processData: false,
                success: ( data, textStatus, jQxhr ) => {
                    window.location.replace("{{ route('ventana1') }}");
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log('errorThrown :', errorThrown);
                }
            });
        })
    </script>
@endsection