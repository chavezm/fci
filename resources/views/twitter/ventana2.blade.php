@extends('layouts.admin.base')
@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
   <link rel="stylesheet" href="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.css" />
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #mapid { height: 300px; width:100%}
</style>

@endsection

@section('content')
    <h1 align="center">Análisis de Sentimientos </h1>
    <h1 align="center">Por Sectores</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <label for="">Seleccione el Sector:</label>
                <select name="sectores" id="sectores" class="form-control">
                {{--  @foreach($zonas as $z)
                    <option value="{{ $z->id }}">{{ $z->nombre }}</option>
                @endforeach  --}}
                @foreach($zonas as $z)
                    <option value="{{ $z->uidsector }}">{{ $z->sector }}</option>
                @endforeach 
                </select>
                <br>
                <label for="">Seleccione la palabra:</label>
                <select name="palabra" id="palabra" class="form-control" multiple>
                @foreach($palabras as $p)
                    <option value="{{ $p }}">{{ $p }}</option>
                @endforeach
                </select>
                <br>
                <a href="#" onclick="myFunction()" class="btn btn-primary" id="btnReporte">Generar Reporte</a>
            </div>
            <div class="col-lg-6">
                <div id="mapid"></div>        
            </div>
        </div>
        <br>
        </div>
        <div class="row">
            <div class="col-lg-6" style="height:100%;">
                <h4 align="center">REPORTE DE  ANÁLISIS DE</h4>
                <h4 align="center">SENTIMIENTO GENERAL</h4>
                <canvas id="bar-chart" width="800" height="450"></canvas>
            </div> 
            <div class="col-lg-6">
                <div style="max-height: 435px !important;overflow-x: scroll;">
                @if(count($pal)> 0)
                    @foreach($arr as $key => $palabra)
                        @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
                        <div id="{{ $key }}">
                            <h4 align="center">REPORTE DE  ANÁLISIS DE</h4>
                            <h4 align="center">SENTIMIENTO GENERAL</h4>
                            <h3 align="center">{{ $key }}</h3>
                            <canvas id="pie-chart{{ $key }}" width="800" height="450"></canvas>
                        </div>
                        <hr>       
                        <br> 
                        @endif
                    @endforeach
                @endif
                </div>
            </div> 
        </div>
    </div> 
    <div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"> Análisis...</h4>
                </div>
                <div class="modal-body"  align="center">
                <img src="{{ asset('img/images/cargando_ajax.gif') }}" alt="">
                
                </div>
            </div>
        </div>
    </div>   
@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.js"></script>
    <script>
        
        $('#btnReporte').click( function() {
            window.location.replace("{{ route('ventana2')}}"+"?zona="+$('#sectores').val()+"&"+"palabra="+$('#palabra').val());
        })
        var mymap = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiYXJtYW5kb2NoYXZleiIsImEiOiJjanNvOTN2c3gwa2VwM3ltbGlranRzaTU5In0.Gp0kaVIbVkMcmXZ56zXkzw'
        }).addTo(mymap);

        @if($zonaReporte != null)
        @php
        echo "let pol = {$zonaReporte->coordenadas};";
        @endphp            
        L.geoJSON(pol).addTo(mymap);
        //var polygon = L.polygon(pol).bindTooltip('{{ $zonaReporte->sector }}').addTo(mymap);
        @endif

        @if( count($pal) != 0 && $zonaReporte != null)
        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: ["Positivo", "Negativo", "Neutral"],
                datasets: [{
                        backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                        data: [
                            {{ $arr['cantPositivos'] }},
                            {{ $arr['cantNegativos'] }},
                            {{ $arr['cantNeutrales'] }}
                        ]                    
                }]
            },
            options: {
                legend: { 
                    display: false 
                    },
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
            }
        });
        @endif
        @if($arr != null)
        @foreach($arr as $key => $palabra)
        @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
        
           /* @foreach($palabra as $p)
                @if($p['data']['geo'] != null)                    
                    createMarker(mymap, {{ $p['data']['geo']['coordinates'][0] }}, {{ $p['data']['geo']['coordinates'][1] }},"{{ $p['data']['class'] }}","{{ $p['data']['user_data']['profile_image_url_https'] }}","{{ $p['data']['user_data']['name'] }}")
                @endif
            @endforeach */
           
            new Chart(document.getElementById("pie-chart{{ $key }}"), {
                type: 'pie',
                data: {
                    labels: ["Positivo", "negativo", "neutral"],
                    datasets: [{
                        backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                        data: [
                                {{ $palabra['cantPositivos'] }},
                                {{ $palabra['cantNegativos'] }},
                                {{ $palabra['cantNeutrales'] }}
                            ]
                            
                    }]
                },
                options: {
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
                }
            });
            @endif
        @endforeach
        @endif
       /* 
        function createMarker(map,lt,lg,description,img,nombre){
            if(description== 'negativo'){
                var url ='https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png'
               }else if(description== 'positivo') {
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png';
               }else{
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png';
               }
               var greenIcon = new L.Icon({
                iconUrl: url ,
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
              });
            L.marker([lt, lg],{icon: greenIcon})
            .bindPopup('<table class="table table-condensed"><tbody><tr><td><img src="'+img+'" height="100" width="100"></td><td>'+description+'</td><td>'+nombre+'</td></tr></tbody></table>')
            .addTo(mymap)
        }
        */
        function myFunction() {
            $('#miModal').modal("show");
        } 
    </script>

@endsection