@extends('layouts.admin.base') 
@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #mapid { height: 300px; width:100%}
</style>

@endsection

@section('content')

    <h1 align="center">Analisis de sentimiento en twitter</h1>
    <h1 align="center">Guayaquil</h1>
    <button href="{{Route('pp')}}" type="button" class="btn btn-default" >Actualizar</button>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="mapid"></div>        
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6">
                <h4 align="center">REPORTE DE  ANALISIS DE</h4>
                <h4 align="center">SENTIMINETO GENERAL</h4>
                <canvas id="bar-chart" width="800" height="450"></canvas>

            </div> 
            <div class="col-lg-6">
                <h4 align="center">REPORTE DE  ANALISIS DE</h4>
                <h4 align="center">SENTIMINETO GENERAL</h4>
                <canvas id="pie-chart" width="800" height="450"></canvas>
            </div> 
        </div>
    </div>    
 
@endsection

@section('scripts')

    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.js"></script>
    <script>
         var mymap = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZGF2ZS1zYWxhemFyIiwiYSI6ImNqcTJtNW5zaDE5cno0M3M3cGY1eWc2NDkifQ.NKsu3DL1RFWfrsn-4BWouQ'
        }).addTo(mymap);
        //loop data

        @foreach($seguidores as $f)
            @if($f->coorlatitud != 'no aplica')
                createMarker(mymap, {{$f->coorlatitud}}, {{$f->coorlongitud}},"{{ $f->sentimiento }}","{{ $f->url }}","{{$f->nombre}}")
            @endif
        @endforeach

        //create marker
        var myIcon = L.divIcon({className: 'my-div-icon'});
        function createMarker(map,lt,lg,description,img,nombre){
            console.log(lt,lg)
            L.marker([lt, lg])
            .bindPopup('<table class="table table-condensed"><tbody><tr><td><img src="'+img+'" height="100" width="100"></td><td>'+description+'</td><td>'+nombre+'</td></tr></tbody></table>')
            .addTo(map)
        }
        //lt"-2.20382"          lg "-79.8975"

       /* new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
            labels: ["Positivo", "Negativo", "Neutral"],
            datasets: [
                {
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                data: [2478,5267,734,784,433]
                }
            ]
            },
            options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Estadisticas de sentimiento'
            }
            }
        });

        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
            labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
            datasets: [{
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [2478,5267,734,784,433]
            }]
            },
            options: {
            title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
            }
            }
        });*/

    </script>
   
@endsection