@extends('layouts.admin.base')
@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin=""/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.css" />
    <style>         
     #mapid { height: 400px; width:100%}

	</style> 
@endsection
@section('content')
    <h1 align="center">Análisis de Sentimientos en Linea</h1>
    <hr>
    <div class="container">
        <div class="row">          
            <div class="col-lg-12">
                <div id="mapid"></div>                      
            </div>
            <div class="col-lg-6">
                <input type="text" name="coordenadas" id="coordsZona" value="" style="visibility: hidden;">
                <input type="text" name="nombre"  style="visibility: hidden;">
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6" style="height:100%;">
            <h4 align="center">REPORTE DE  ANÁLISIS DE</h4>
            <h4 align="center">SENTIMIENTO GENERAL</h4>
            <canvas id="bar-chart" width="800" height="450"></canvas>
        </div> 
        <div class="col-lg-6">
            <div id="divEstadisticas" style="max-height: 435px !important;overflow-x: scroll;">

            </div>
        </div> 
    </div>
</div>
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"> Análisis...</h4>
            </div>
            <div class="modal-body"  align="center">
            <img src="{{ asset('img/images/cargando_ajax.gif') }}" alt="">
            
            </div>
        </div>
    </div>
</div>    
@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.js"></script>
    <script>
        $('#frmNombre').submit(function (e) {
            e.preventDefault()
            $("#myModal").modal('hide');
        })
        var map = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiYXJtYW5kb2NoYXZleiIsImEiOiJjanNvOTN2c3gwa2VwM3ltbGlranRzaTU5In0.Gp0kaVIbVkMcmXZ56zXkzw'
        }).addTo(map);


            
        // Initialise the FeatureGroup to store editable layers
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        // Initialise the draw control and pass it the FeatureGroup of editable layers
        var drawControl = new L.Control.Draw({
            
            edit: {
                featureGroup: drawnItems
            },
            draw: {
                polyline : false,
                rectangle : false,
                circle : true,
                marker: false,
                circlemarker: false,
                polygon: false
            }
        });

        map.addControl(drawControl);

        map.on(L.Draw.Event.CREATED, function (e) {
           
            var type = e.layerType
            var layer = e.layer;      
            drawnItems.addLayer(layer);   
            drawControl.remove();
            var data = {
                lat: layer._latlng.lat,
                lng: layer._latlng.lng,
                kmRadius: (layer._mRadius*0.0001).toFixed(2)
            }
            $.ajax({
                url: '{{ route('ventana3Post') }}',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                processData: false,
                beforeSend: function(){
                    $('#miModal').modal("show");
                },
                success: ( data, textStatus, jQxhr ) => {
                    // GRAFICO DE Barras
                    $('#miModal').modal("hide");
                    new Chart(document.getElementById("bar-chart"), {
                        type: 'bar',
                        data: {
                            labels: ["Positivo", "Negativo", "Neutral"],
                            datasets: [{
                                    backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                                    data: [
                                        data.cantPositivos,
                                        data.cantNegativos,
                                        data.cantNeutrales
                                    ]                    
                            }]
                        },
                        options: {
                            legend: { 
                                display: false 
                                },
                            title: {
                                display: true,
                                text: 'Estadisticas de sentimiento (Cantidad)'
                            }
                        }
                    });
                    //GRAFICO DE PASTEL
                    for( el in data) {
                       if(el !== "cantPositivos" && el !== "cantNegativos" && el !== "cantNeutrales" ){
                            $('#divEstadisticas').append(` 
                                <div>
                                    <h4 align="center">REPORTE DE  ANÁLISIS DE</h4>
                                    <h4 align="center">SENTIMIENTO GENERAL</h4>
                                    <h3 align="center">${el}</h3>
                                    <canvas id="pie-chart${el}" width="800" height="450"></canvas>
                                </div>
                            `);

                            for( element in data[el]) {
                                if(data[el][element].data != null){
                                    if(data[el][element].data.geo !== null){
                                        createMarker(map, data[el][element].data.geo.coordinates[0], data[el][element].data.geo.coordinates[1], data[el][element].data.class, data[el][element].data.user_data.profile_image_url_https, data[el][element].data.user_data.name);
                                    }
                                }
                                
                            }

                            new Chart(document.getElementById(`pie-chart${el}`), {
                                type: 'pie',
                                data: {
                                    labels: ["Positivo", "negativo", "neutral"],
                                    datasets: [{
                                        backgroundColor: ["#3e95cd", "#EA0808","#3cba9f"],
                                        data: [
                                                data[el].cantPositivos,
                                                data[el].cantNegativos,
                                                data[el].cantNeutrales
                                            ]                                            
                                    }]
                                },
                                options: {
                                title: {
                                    display: true,
                                    text: 'Estadisticas de sentimiento (Cantidad)'
                                }
                                }
                            });
                       }
                    }
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    
                }
            });
        });
       
        
        function createMarker(map,lt,lg,description,img,nombre){
            if(description== 'negativo'){
                var url ='https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png'
               }else if(description== 'positivo') {
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png';
               }else{
                var url = 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png';
               }
            
            var greenIcon = new L.Icon({
                iconUrl: url ,
                shadowUrl: '',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
              });
            L.marker([lt, lg],{icon: greenIcon})
            .bindPopup('<table class="table table-condensed"><tbody><tr><td><img src="'+img+'" height="100" width="100"></td><td>'+description+'</td><td>'+nombre+'</td></tr></tbody></table>')
            .addTo(map)
        }
        function myFunction() {
            $('#miModal').modal("show");
        }
       
    </script>
@endsection