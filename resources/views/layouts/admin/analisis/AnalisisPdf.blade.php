<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Analisis Kmeans</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
    @if($kmrlatitud != null)
    <div class="container">
        <div class="row" style="text-align: center" >
            <div class="col-md-12" style="padding-bottom: 5%">
                    <h1 class="text-primary" >Analisis Kmeans (R)</h1>
                    <p class="text-secondary" style="text-align: justify">El algoritmo K-means en un método de agrupamiento de puntos, que tiene como objetivo la repartición de un conjunto de n observaciones en k conjuntos de grupos en el que cada observación importa al grupo de cuyo valor medio es más cercano. </p>
            </div>
        </div>
          <div class="row">
                <div class="col-md-12" style="text-align: center">
                        <img src="img/images/analisis.png" style="height: 50%; width: 70%">
                </div>

        </div>
        <div class="row">
            <div class="col-md-12" style="padding-top: 5%">
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Cluster</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                          </tr>
                        </thead>
                        <tbody>
                        @for($i=0;$i<count($kmrlatitud);$i++)
                         <tr>
                            <th scope="row">{{ $i+1 }}</th>
                            <td>{{ $kmrlatitud[$i] }}</td>
                            <td>{{ $kmrlongitud[$i] }}</td>
                         </tr>
                        @endfor
                        </tbody>
                      </table>    
            </div>
        </div> 
    </div>
    @endif

    @if($kmplatitud != null)
    <div style="page-break-after:always;"></div>
    <div class="container">
        <div class="row" style="text-align: center" >
            <div class="col-md-12" style="padding-bottom: 5%">
                    <h1  class="text-primary">Analisis Kmeans (Python)</h1>
                    <p class="text-secondary" style="text-align: justify">La técnica de análisis cluster o análisis de conglomerados consiste en clasificar a los
individuos en estudio formando grupos o conglomerados (cluster) de elementos, tales
que los individuos dentro de cada conglomerado presenten cierto grado de
homogeneidad en base a los valores adoptados sobre un conjunto de variables. </p>
            </div>
        </div>
          <div class="row">
                <div class="col-md-12" style="text-align: center">
                        <img src="img/images/KmeansCal.png" style="height: 50%; width: 70%">
                </div>

        </div>
        <div class="row">
            <div class="col-md-12" style="padding-top: 5%">
                <table class="table table-striped   ">
                        <thead>
                          <tr>
                            <th scope="col">Cluster</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                          </tr>
                        </thead>
                        <tbody>
                        @for($i=0;$i<count($kmplatitud);$i++)
                         <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $kmplatitud[$i] }}</td>
                            <td>{{ $kmplongitud[$i] }}</td>
                         </tr>
                        @endfor
                        </tbody>
                      </table>    
            </div>
        </div> 
    </div>
    @endif
   
    @if($dbslatitud != null)
    <div style="page-break-after:always;"></div>
    <div class="container">
        <div class="row" style="text-align: center" >
            <div class="col-md-12" style="padding-bottom: 5%">
                    <h1  class="text-primary">Analisis DBS (Python)</h1>
                    <p class="text-secondary" style="text-align: justify">El algoritmo Dbscan se basa en la densidad de aplicaciones con ruido, por lo que se considera como un algoritmo de agrupamiento de los datos. Este descubre el número de grupos ya comenzados por la distribución de la densidad de los nodos correspondientes.
                    </p>
            </div>
        </div>
          <div class="row">
                <div class="col-md-12" style="text-align: center">
                        <img src="img/images/dbScanCal.png" style="height: 50%; width: 70%">
                </div>

        </div>
        <div class="row">
            <div class="col-md-12" style="padding-top: 5%">
                <table class="table table-striped   ">
                        <thead>
                          <tr>
                            <th scope="col">Cluster</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                          </tr>
                        </thead>
                        <tbody>
                        @for($i=0;$i<count($dbslatitud);$i++)
                         <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $dbslatitud[$i] }}</td>
                            <td>{{ $dbslongitud[$i] }}</td>
                         </tr>
                        @endfor
                        </tbody>
                      </table>    
            </div>
        </div> 
    </div>
    @endif

    @if($hcelatitud != null)
    <div style="page-break-after:always;"></div>
    <div class="container">
        <div class="row" style="text-align: center" >
            <div class="col-md-12" style="padding-bottom: 5%">
                    <h1  class="text-primary">Analisis HCE (Python)</h1>
                    <p class="text-secondary" style="text-align: justify">El agrupamiento jerárquico estructurado determina el número correcto de clusters,  ya que se da un poco control sobre los procesos de agrupamiento. Con los clusters se determinara la agrupación natural correspondiente y los controles dinámicos. Estas  particiones son el conjunto de datos por niveles, por lo que cada nivel une o divide los grupos del nivel anterior.</p>
            </div>
        </div>
          <div class="row">
                <div class="col-md-12" style="text-align: center">
                        <img src="img/images/hceCal.png" style="height: 50%; width: 70%">
                </div>

        </div>
        <div class="row">
            <div class="col-md-12" style="padding-top: 5%">
                <table class="table table-striped   ">
                        <thead>
                          <tr>
                            <th scope="col">Cluster</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                          </tr>
                        </thead>
                        <tbody>
                        @for($i=0;$i<count($hcelatitud);$i++)
                         <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $hcelatitud[$i] }}</td>
                            <td>{{ $hcelongitud[$i] }}</td>
                         </tr>
                        @endfor
                        </tbody>
                      </table>    
            </div>
        </div> 
    </div>
    @endif

    
    @if($hcnelatitud != null)
    <div style="page-break-after:always;"></div>
    <div class="container">
        <div class="row" style="text-align: center" >
            <div class="col-md-12" style="padding-bottom: 5%">
                    <h1  class="text-primary">Analisis HCNE (Python)</h1>
                    <p class="text-secondary" style="text-align: justify"> El agrupamiento jerárquico no  estructurado no determina el número correcto de clúster, ya que en este se da menos control sobre los procesos de agrupamiento. Con los clúster podrán determinar la agrupación natural correspondiente y los controles dinámicos. </p>
            </div>
        </div>
          <div class="row">
                <div class="col-md-12" style="text-align: center">
                        <img src="img/images/hcneCal.png" style="height: 50%; width: 70%">
                </div>

        </div>
        <div class="row">
            <div class="col-md-12" style="padding-top: 5%">
                <table class="table table-striped   ">
                        <thead>
                          <tr>
                            <th scope="col">Cluster</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                          </tr>
                        </thead>
                        <tbody>
                        @for($i=0;$i<count($hcnelatitud);$i++)
                         <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $hcnelatitud[$i] }}</td>
                            <td>{{ $hcnelongitud[$i] }}</td>
                         </tr>
                        @endfor
                        </tbody>
                      </table>    
            </div>
        </div> 
    </div>
    @endif
</body>
</html>