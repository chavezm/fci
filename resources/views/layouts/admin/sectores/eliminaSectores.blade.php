@extends('layouts.admin.base')
@section('title', 'Configuracion Roles')

    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>

@section('content')

<body>
<div class="row">
  <div class="col-md-3">
    <div class="x_panel">
    <form method="GET" class="form-horizontal form-label-left" >
      
      <h1 align="center">Lesstraffic</h1>
      <table>
        <tr>
          <td><legend><label>Eliminar sectores:</label></legend></td><br>
        </tr>
      </table>
      <table>
            
        <?php  foreach($sectorp as $sector){  ?>
            <?php if($sector->estado == "A"){?>
            <tr> 
                <td> <?= $sector->sector;?></td>   
                <td>&emsp;|&emsp;</td>
                <td><a href="eliminar_sector?uidsector=<?= $sector->uidsector;?>" class="btn btn-primary">Eliminar</a><br></td>
                
            </tr>

            <?php } ?>
        <?php } ?>
      </table>
    </form>
    </div>
  </div>
  <div class="col-md-9">
    <div class="x_panel">

    <div id="map" style="width: 800px; height: 600px; border: 1px solid #ccc"></div>
        
    <script>

        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            osm = L.tileLayer(osmUrl, { maxZoom: 18, attribution: osmAttrib }),
            map = new L.Map('map', { center: new L.LatLng(-2.1887106287772053,-79.89135503768922), zoom: 12 }),
            drawnItems = L.featureGroup().addTo(map);

    L.control.layers({
        'osm': osm.addTo(map),
        "google": L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
            attribution: 'google'
        })
    }, { 'drawlayer': drawnItems }, { position: 'topleft', collapsed: false }).addTo(map);
    <?php  foreach($sectorp as $sector){  ?>
        <?php if($sector->estado == "A"){?>
        var coordenadas = new L.geoJson(<?= $sector->coordenadas;?>).addTo(map); 
        var puntomedio = new L.geoJson(<?= $sector->puntomedio;?>).addTo(map);
        var arreglo = ["<?= $sector->sector;?>",JSON.stringify(<?= $sector->coordenadas;?>)];
        coordenadas.bindPopup(arreglo[0],arreglo[1]);
        <?php } ?>
    <?php } ?>

    </script>
   
  </div>
</div>

</body>

@endsection