@extends('layouts.admin.base')
@section('title', 'Configuracion Roles')

    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>

@section('content')

<body>
<div class="row">
  <div class="col-md-3">
    <div class="x_panel">
    <form method="GET" class="form-horizontal form-label-left" >
      
      <h1 align="center">Lesstraffic</h1>
      <table>
        <tr>
          <td><legend><label>Seleccione el sector que desea analizar: </label></legend></td><br>
        </tr>
      </table>
      <table id="table">
        <?php  foreach($sectorp as $sector){  ?>
            <?php if($sector->estado == "A"){?>
                <input type="checkbox" nombre="mycheck" id="<?= $sector->uidsector;?>" value="<?= $sector->uidsector;?>" >  <?= $sector->sector;?></label><br>
            <?php } ?>
        <?php  }   ?>
        <td><a onclick="submitCheck()" class="btn btn-primary">Kmeans</a><br></td>

      </table>
    

    </form>
    </div>
  </div>
  <div class="col-md-9">
    <div class="x_panel">

    <div id="map" style="width: 800px; height: 600px; border: 1px solid #ccc"></div>
        
    <script>
    var arrayselect = new Array();

        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            osm = L.tileLayer(osmUrl, { maxZoom: 18, attribution: osmAttrib }),
            map = new L.Map('map', { center: new L.LatLng(-2.1887106287772053,-79.89135503768922), zoom: 12 }),
            drawnItems = L.featureGroup().addTo(map);

    L.control.layers({
        'osm': osm.addTo(map),
        "google": L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
            attribution: 'google'
        })
    }, { 'drawlayer': drawnItems }, { position: 'topleft', collapsed: false }).addTo(map);
    <?php  foreach($sectorp as $sector){  ?>
        <?php if($sector->estado == "A"){?>
        var coordenadas = new L.geoJson(<?= $sector->coordenadas;?>).addTo(map); 
        var puntomedio = new L.geoJson(<?= $sector->puntomedio;?>).addTo(map);
        var arreglo = ["<?= $sector->sector;?>",JSON.stringify(<?= $sector->coordenadas;?>)];
        coordenadas.bindPopup(arreglo[0],arreglo[1]);
        <?php } ?>
    <?php } ?>


    function submitCheck(){
        var x =0;
        var ids='';
        var size = $("input:checked").length;
       $("input:checked").each(function(){
          if($(this).attr('nombre')==='mycheck'){
            ids+=$(this).attr('id')+',';
            x++;
          }
         
        });
       if(x > 0){
             location.href = "Sectorsel/"+ids;
       }else{
        alert("Por favor elija al menos un sector...");
       }
      
    }

    </script>
   
  </div>
</div>

</body>

@endsection