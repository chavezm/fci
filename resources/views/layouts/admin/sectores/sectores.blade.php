@extends('layouts.admin.base')
@section('title', 'Configuracion Roles')
    
    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>
    
@section('content')

<body>
<div class="row">
  <div class="col-md-3">
    <div class="x_panel">
    <form method="GET" class="form-horizontal form-label-left" >
      

      <h1 align="center">Lesstraffic</h1>
      <table>
        <tr>
          <td><label><i class="fas fa-draw-polygon"></i>  Crear nuevo sector :</label></td><br>
        </tr>
      </table>
    </form>
    
    <form  id="f_nuevo_sector"  method="post"  action="agregar_nuevo_sector" class="form-horizontal form_entrada tipo" > 
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  
        <div class="form-group col-xs-6">
            <label for="sector">sector</label>
            <input type="text" class="form-control" id="sector" name="sector" >
        </div>

        <div class="form-group col-xs-6">
            <label for="uidciudad">uidciudad</label>
            <input type="text" class="form-control" id="uidciudad" name="uidciudad"  value=1 readonly style="">
        </div>
  
        <div class="form-group col-xs-6">
            <label for="comentario">comentario</label>
            <input type="text" class="form-control" id="comentario" name="comentario" >
        </div>
  
        <div class="form-group col-xs-6">
            <label>Coordenadas</label>
            <input type="text" class="form-control" id="coordenadas" name="coordenadas" value="" readonly style="">
        </div>

        <div class="form-group col-xs-6">
            <label>Punto medio</label>
            <input type="text" class="form-control" id="puntomedio" name="puntomedio" value="" readonly style="">
        </div>

        <div class="form-group col-xs-6">
            <label>Estado</label>
            <input type="text" class="form-control" id="estado" name="estado" value="A" readonly style="">
        </div>
  
        <div class="box-footer col-xs-12 ">
            <td>
                <button type="submit" class="btn btn-primary">Guardar</button> 
            </td>
            
            <td>
                <a href="{{ url('admin/sectores/sectores') }}" class="btn btn-primary">Recargar</a>
            </td>
        </div>
      </form>
 
    </div>
  </div>
  <div class="col-md-9">
    <div class="x_panel">

    <div id="map" style="width: 800px; height: 600px; border: 1px solid #ccc"></div>
        
    <script>
        var arregloCoordenadas =  new Array();
        var arregloNombreSector= new Array();
        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            osm = L.tileLayer(osmUrl, { maxZoom: 18, attribution: osmAttrib }),
            map = new L.Map('map', { center: new L.LatLng(-2.1887106287772053,-79.89135503768922), zoom: 12 }),
            drawnItems = L.featureGroup().addTo(map);

    L.control.layers({
        'osm': osm.addTo(map),
        "google": L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
            attribution: 'google'
        })
    }, { 'drawlayer': drawnItems }, { position: 'topleft', collapsed: false }).addTo(map);
    <?php  foreach($sectorp as $sector){  ?>
        <?php if($sector->estado == "A"){?>
            arregloCoordenadas.push(<?= $sector->coordenadas;?>);
            arregloNombreSector.push("<?= $sector->sector;?>");
            var coordenadas = new L.geoJson(<?= $sector->coordenadas;?>).addTo(map); 
            var puntomedio = new L.geoJson(<?= $sector->puntomedio;?>).addTo(map);
            var arreglo = ["<?= $sector->sector;?>",JSON.stringify(<?= $sector->coordenadas;?>)];
            coordenadas.bindPopup(arreglo[0],arreglo[1]);
        <?php } ?>
    <?php } ?>

    //ESTRUCTURA QUE PERMITE DIBUJAR LAS FIGURAS EN EL MAPA Y DEFINIR SU COLOR...
    var drawControlFull = new L.Control.Draw({
        draw: {
            rectangle: {
                shapeOptions: {
                    color: 'lime'
                }
            },
            circlemarker: false,
            marker : false,
            circle : false,
            polyline: false,
            polygon: {
                shapeOptions: {
                    color: 'lime'
                },  
                allowIntersection: false,
                showArea: true
            }
        }
    });
    map.addControl(drawControlFull);

    var coordenadas;
    map.on("draw:created", function (e) {
        var insertarPoligono = false;
        var type = e.layerType;
        var layer = e.layer;
        var shape = layer.toGeoJSON()
        var cnt = arregloCoordenadas.length;
        //CICLO FOR QUE VERIFICA SI LAS COORDENADAS DEL POLIGONO QUE ESTAMOS DIBUJANDO INTERSECTAN A OTROS POLIGONOS EXISTENTES...
        for(var i=0; i < cnt; i++)
        {
            var intersection = turf.intersect(shape, arregloCoordenadas[i]);
            if(intersection != null){
                alert("EL NUEVO POLIGONO SE INTERSECTA CON EL POLIGONO: "+arregloNombreSector[i]);
                insertarPoligono = true;
            }
        }
        //CONDICIONAL QUE INGRESA LOS DATOS AL FORMULARIO SI EL NUEVO SECTOR NO SE INTERSECTA CON OTROS POLIGONOS EXISTENTES...
        if(insertarPoligono != true)
        {
            var cen2 = turf.centroid(shape);
            L.geoJson(cen2).addTo(map);
            var puntomedio = JSON.stringify(cen2);
            //alert(puntomedio);
            document.getElementById("puntomedio").value = puntomedio;
            coordenadas = JSON.stringify(shape);
            //alert(coordenadas);
            document.getElementById("coordenadas").value = coordenadas;
            drawnItems.addLayer(layer).bindPopup(coordenadas);//texto en los poligonos...
            drawControlFull.setDrawingOptions({
                polygon:false
            });
            //REMOVEMOS EL PANEL DE DIBUJO UNA VES QUE SE CREER UN NUEVO SECTOR...
            map.removeControl(drawControlFull);
        }else{
            alert("NO SE PUEDEN INSERTAR POLIGONOS QUE INTERSECTEN A OTROS YA EXISTENTES...");
        }
    }); 
    </script>
   
  </div>
</div>

</body>

@endsection