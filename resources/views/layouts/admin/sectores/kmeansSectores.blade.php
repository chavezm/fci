@extends('layouts.admin.base')
@section('title', 'Configuracion Roles')

    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>

    <!-- SCRIPT QUE PERMITE MOSTRAR LAS ALERTAS CON ICONOS....   -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- SCRIPT QUE PERMITE MOSTRAR LAS ALERTAS CON ICONOS....   -->

@section('content')

<body>
<div class="row">
  <div class="col-md-4">
    <div class="x_panel">
    <form method="GET" class="form-horizontal form-label-left" >
      

      <h1 align="center">Lesstraffic</h1>
      <table>
        <tr>
          <td><label>Análisis kmeans sobre sectores :</label></td><br>
        </tr>
      </table>

      <table class=" table table-borderless"> 
            <tr>
              <td colspan="2"><label>Fecha: </label>
              <input class="form-control" type="date" name="fecha_desde" id="fecha_desde"  value="2019-01-01"  step="1"></td>           
            </tr>
            <tr>
              <td colspan="2"><label>Hora: </label>
              <input class="form-control" id="hora" type="time" name="hora" min="10:00" max="22:00" value="12:00"></td> 
            </tr>
            <tr>
              <td colspan="2">  
                <label>Base de datos: </label>
                <select name="bd" id="bd">
                <option value="1">Base de datos simulada </option>
                <option value="2">Base de datos real </option>                    
                </select>
              </td>            
            </tr>        
            <tr>
              <td colspan="2">
                <label>Num. de Clusters: </label>
                <select name="num_cluster" id="num_cluster">
                <option value="1">1 Cluster</option>
                <option value="2">2 Cluster</option>
                <option value="3">3 Cluster</option>
                <option value="4">4 Cluster</option>
                <option value="5">5 Cluster</option>
                <option value="6">6 Cluster</option>
                <option value="7">7 Cluster</option>
                <option value="8">8 Cluster</option>
                <option value="9">9 Cluster</option>
                <option value="10">10 Cluster</option>
                </select>
              </td>
            </tr>          
            <tr>        
              <td align="r" colspan="2" >
                <input class="btn btn-primary btn-sm btn-block" type="button" value="Cargar Puntos" name="bt_aceptar"  align="center" onclick="carga_puntos_map();"/>            
              </td>
            </tr>
            <tr>
                <td align="r" colspan="2">
                <input class="btn btn-primary btn-sm btn-block" type="button" value="Nuevo" name="bt_limpiar"  align="center" onclick="window.location.reload()"/>
                </td>
            </tr>

            <tr>   
                <td align="r" colspan="2" >
                    <a href="{{ url('admin/sectores/elegirSectorKmeans') }}" class="btn btn-primary btn-sm btn-block">Elegir nuevos sectores</a> 
                </td>        
            </tr>   
            <!-- <tr>
                 <td align="r" colspan="2">
                    <input class="btn btn-primary btn-sm btn-block" type="button" value="Exportar Pdf" name="bt_exportar"  align="center" onclick="exportar_pdf();"/>
                </td>
            </tr> -->
            <tr>
              <th style="text-align: center" colspan="2">
                <h5>ALGORITMOS</h5>
              </th>
            </tr>
         
            <tr>   
              <td style="text-align: center" align="r" colspan="2" >
                  <div class="btn-group" role="group" aria-label="Basic example">
                  <input class="btn btn-sm btn-warning" type="button" value="KMEANS(R)"   name="bt_analisis" align="center" onclick="ajax_r();"/>
                    <!-- <input class="btn btn-sm btn-warning" type="button" value="KMEANS"    name="bt_kmean"  id="bt_kmean"  align="center" onclick="nn(1);"/>
                    <input class="btn btn-sm btn-warning" type="button" value="DBSCAN"    name="bt_dbscan"  id="bt_dbscan"  align="center" onclick="nn(2);"/>
                    <input class="btn btn-sm btn-warning" type="button" value="HCN"    name="bt_hcn"  id="bt_hcn"  align="center" onclick="nn(3);"/>
                    <input class="btn btn-sm btn-warning" type="button" value="HCNE"    name="bt_hcne"  id="bt_hcne"  align="center" onclick="nn(4);"/>   -->       
                 
                  </div>
              </td>        
            </tr>  
    
          </table>
    </form>

    <h3>Color del sector:</h3>
                    <div class="p-3 mb-2 bg-danger text-white">Sector/es con mayor numero de vehiculos.
                        <img src="{{ asset('img/images/leaf-red.png') }}" id="red" class="img-responsive">
                    </div>
                    <div class="p-3 mb-2 bg-warning text-dark">Sector/es con el numero medio de vehiculos.
                        <img src="{{ asset('img/images/leaf-orange.png') }}" id="red" class="img-responsive">
                    </div> 
                    <div class="p-3 mb-2 bg-success text-white">Sector/es con menor numero de vehiculos.
                        <img src="{{ asset('img/images/leaf-green.png') }}" id="red" class="img-responsive">
                    </div>   
    <!--IMAGEN EN FORMA DE CIRCULO QUE DICE CARGANDO...-->
    <img src="{{ asset('img/images/cargando_ajax.gif') }}" id="imagen" class="img-responsive" style="height: 450px;visibility: hidden;">
 
    </div>
  </div>
  <div class="col-md-8">
    <div class="x_panel">

    <div id="map" style="width: 800px; height: 600px; border: 1px solid #ccc"></div>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>

var latitud;
var longitud;
var cont_vehiculos = 0;
var bandera_historica;
var bandera_analisis;
var bandera_exportacion_pdf;
var latlngs = new Array();
var latlngs_data = new Array();
var arr_lat_lng = new Array();
var latlngA; var lat; var cont = 0;
var latlngB; var lng; var cont_funcion = 0;
var fecha_desde;
var fecha_hasta;
var usuario = "{{ Auth::user()->id }}";
coords = [];
var layerLatLong;                                   //DOUGLAS NATHA / CESAR PINELA
var x,z = 0,w = 0;
var coordenadas;
var may = 0,men = 1000,med = 0;
var arr_cantidadPuntosporSector = new Array();
var count_result_aux;

    
        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            osm = L.tileLayer(osmUrl, { maxZoom: 18, attribution: osmAttrib }),
            map = new L.Map('map', { center: new L.LatLng(-2.1887106287772053,-79.89135503768922), zoom: 12 }),
            drawnItems = L.featureGroup().addTo(map);

    L.control.layers({
        'osm': osm.addTo(map),
        "google": L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
            attribution: 'google'
        })
    }, { 'drawlayer': drawnItems }, { position: 'topleft', collapsed: false }).addTo(map);
    //EN ESTA PARTE INSERTO MI POLIGONO...
    <?php  foreach($sectorp as $sector){  ?>
        <?php if($sector->estado == "A"){?>
        coordenadas = new L.geoJson(<?= $sector->coordenadas;?>,{
        onEachFeature(feature, layer){
                layer.bindPopup("<?= $sector->sector;?>");
                coords.push(feature.geometry.coordinates);
                layerLatLong = layer.getLatLngs();
        }  
        }).addTo(map);
        var alerta = JSON.stringify(<?= $sector->coordenadas;?>);
        <?php } ?>
    <?php } ?>


    
    function carga_puntos_map() {   
    console.log(cont_funcion);
    console.log("carga_puntos_map");
    fecha_desde = $('#fecha_desde').val();
    origen = $('#bd').val();
    hora = $('#hora').val();
    latlngA = arr_lat_lng[0];
    latlngB = arr_lat_lng[1];

    if (fecha_desde != "" /*&& (latlngA != null && latlngB != null)*/ && cont_funcion == 0) {
        bandera_analisis = true;
        //swal("Datos correctos!", "", "success");
        cont_funcion = cont_funcion + 1;
        carga_puntos(latlngA, latlngB, fecha_desde,hora,origen);
    }
    else if (fecha_desde == "" || fecha_hasta == "") {
        swal("", "Ingrese los valores de la fecha!", "success", { icon: "warning", });
    }
    // else if (latlngA == null || latlngB == null) {
    //     swal("", "Limite la zona, por favor!", "success", { icon: "warning", });
    // }
}

    function carga_puntos(latlngA, latlngB, fecha_desde,hora,origen) {
    $.ajax(
        {
            type: "GET",
            url: "../ajax_carga_data/" + fecha_desde + "/" + hora+ "/" + origen,
            beforeSend: function () {
                console.log("cargando...");
             
            },
            success: function (result) {
                //document.getElementById('imagen').style.visibility = 'hidden';
                var JsonResult = result;
                console.log(JsonResult);
                var count_result = JsonResult.length;
               
                // alert("prueba");
               /* for(i in JsonResult){
                    console.log(JsonResult[i].id);
                    console.log(JsonResult[i].latitud);
                   // console.log(i[cont][2]);
                    cont++;
                }*/
                //alert(count_result);
                count_result_aux = count_result;
                //var result2 = prueb(coords,100,100);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //SE INSERTA EL FOREACH ANTES DEL CICLO FOR PARA QUE EVALUE TODOS LOS POLIGONOS QUE SE ENCUENTREN ACTIVOS
                <?php  foreach($sectorp as $sector){  ?>
                    <?php if($sector->estado == "A"){?>

                    coordenadas = new L.geoJson(<?= $sector->coordenadas;?>,{
                    onEachFeature(feature, layer){
                            layerLatLong = layer.getLatLngs();
                            for (i = 0; i < count_result; i++) {
                                var id_user = JsonResult[i].id_transporte;//ID user
                                var lat_d = JsonResult[i].latitud;//latitud
                                var lng_d = JsonResult[i].longitud;//longitud
                                //Valida si los puntos extraidos en la base de datos existen en la limitacion de la zona.
                                var point = new L.LatLng(lat_d, lng_d);
                                //PROCESO QUE DETERMINA SI EL PUNTO SE ENCUENTRA DENTRO DEL POLIGONO...
                                  var inside = false; 
                                  var x = point.lat, y = point.lng; 
                                  for (var ii=0;ii<layerLatLong.length;ii++){ 
                                   var polyPoints = layerLatLong[ii]; 
                                   for (var ip = 0, j = polyPoints.length - 1; ip < polyPoints.length; j = ip++) { 
                                    var xi = polyPoints[ip].lat, yi = polyPoints[ip].lng; 
                                    var xj = polyPoints[j].lat, yj = polyPoints[j].lng; 

                                    var intersect = ((yi > y) != (yj > y)) 
                                     && (x < (xj - xi) * (y - yi)/(yj - yi) + xi); 
                                    if (intersect) inside = !inside; 
                                   } 
                                  } 
                                //SI EL PUNTO SE ENCUENTRA DENTRO DEL POLIGONO EL PROCESO DEVOLVERA TRUE O FALSE EN LA VARIABLE INSIDE...
                                            if(inside){
                                                z++;
                                            }                                           
                            }
                    }
                    });
                   
                    //ALGORITMO QUE PERMITE CONOCER QUE SECTOR TIENE EL MAYOR Y EL MENOR NUMERO DE VEHICULOS
                    if(z>may){
                        may = z;
                    }
                    if(z<men){
                        men=z;
                    }
                    z = 0;
                    arr_cantidadPuntosporSector[w] = z; 
                    w++;
                    <?php } ?>
                <?php } ?>


                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                <?php  foreach($sectorp as $sector){  ?>
                    <?php if($sector->estado == "A"){?>
                        L.geoJson(<?= $sector->coordenadas;?>,{
                            onEachFeature(feature, layer){
                            layerLatLong = layer.getLatLngs();
                            /////////////////////////////////////////////////////////////////////////////////////////////////////
                            var id_sector = <?= $sector->uidsector;?>;
                                for (i = 0; i < count_result; i++) {
                                    var id_user = JsonResult[i].id_transporte;//ID user
                                    var lat_d = JsonResult[i].latitud;//latitud
                                    var lng_d = JsonResult[i].longitud;//longitud

                                    //Valida si los puntos extraidos en la base de datos existen en la limitacion de la zona.
                                    var point = new L.LatLng(lat_d, lng_d);
                                    //PROCESO QUE DETERMINA SI EL PUNTO SE ENCUENTRA DENTRO DEL POLIGONO...
                                      var inside = false; 
                                      var x = point.lat, y = point.lng; 
                                        for (var ii=0;ii<layerLatLong.length;ii++){ 
                                        var polyPoints = layerLatLong[ii]; 
                                           for (var ip = 0, j = polyPoints.length - 1; ip < polyPoints.length; j = ip++) { 
                                            var xi = polyPoints[ip].lat, yi = polyPoints[ip].lng; 
                                            var xj = polyPoints[j].lat, yj = polyPoints[j].lng; 

                                            var intersect = ((yi > y) != (yj > y)) 
                                             && (x < (xj - xi) * (y - yi)/(yj - yi) + xi); 
                                            if (intersect) inside = !inside; 
                                           } 
                                        } 
                               
                                                if(inside){
                                                    var point_data = new Array();
                                                    point_data[0] = id_user;
                                                    point_data[1] = lat_d;
                                                    point_data[2] = lng_d;
                                                    point_data[3] = id_sector;
                                                    latlngs_data.push(point_data);
                                                    z++;
                                                }                                           
                                }     
                            }
                        });
                         //ESTRUCTURA QUE PERMITE PINTAR DE COLORES LOS SECTORES DEPENDIENDO DE LA CANTIDAD DE PUNTOS EN CADA SECTOR...
                        L.geoJson(<?= $sector->coordenadas;?>,{
                            style: function(feature) {
                                if(z == may){
                                    return {weight: 3, color: 'red', opacity: 1, fillColor: 'red', fillOpacity: 0.2};
                                }else if(z == men ){
                                    return {weight: 3, color: 'green', opacity: 1, fillColor: 'green', fillOpacity: 0.2}; 
                                }else{
                                    return {weight: 3, color: 'yellow', opacity: 1, fillColor: 'yellow', fillOpacity: 0.2};    
                                }
                            },
                            onEachFeature(feature, layer){
                                    layer.bindPopup("<?= $sector->sector;?>");
                            }  
                        }).addTo(map);
                    z = 0;
                    <?php } ?>
                <?php } ?>
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                console.log(latlngs_data);
                capa_point(latlngs_data);
                insertar_datos(latlngs_data, usuario);
                if (latlngs_data.length > 0) {
                    // capa_point(latlngs_data);
                    //insertar_datos(latlngs_data, usuario);
                }
                else {
                    swal("No existen datos en sector/es...", "Actualice la fecha y hora o eliga otros sector.!", "success", { icon: "warning", });
                    document.getElementById('imagen').style.visibility = 'hidden';
                }
            },
            error: function (result) {
                swal("", "Error en la carga masiva de datos.!", "success", { icon: "warning", });
                document.getElementById('imagen').style.visibility = 'hidden';
            }
        });
    }


    /*
************************************************
********CARACTERISTICA DE LOS MARCADORES********
************************************************
*/
var Icon_data = L.Icon.extend({
    options: {
        iconSize: [10, 10], // size of the icon [38, 95]
    }
});
var Icon_all = L.Icon.extend({
    options: {
        //shadowUrl: 'images/marker_shadow.png',
        iconSize: [28, 35], // size of the icon [38, 95]
        //shadowSize:   [50, 64], // size of the shadow [50, 64]
        iconAnchor: [12, 34], // point of the icon which will correspond to marker's location [22, 94]
        //shadowAnchor: [4, 62],  // the same for the shadow [4, 62]
        //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor [-3, -76]
    }
});
var Icon_moto = new Icon_data({ iconUrl: "{{ asset('img/images/p1.png') }}" }),
    Icon_colectivo = new Icon_data({ iconUrl: "{{ asset('img/images/p2.png') }}" }),
    Icon_auto = new Icon_data({ iconUrl: "{{ asset('img/images/p3.png') }}" }),
    Icon_motoneta = new Icon_data({ iconUrl: "{{ asset('img/images/p4.png') }}" }),
    Icon_bicicleta = new Icon_data({ iconUrl: "{{ asset('img/images/p5.png') }}" }),
    Icon_taxi_informal = new Icon_data({ iconUrl: "{{ asset('img/images/p6.png') }}" }),
    Icon_camioneta = new Icon_data({ iconUrl: "{{ asset('img/images/p7.png') }}" }),
    Icon_furgoneta = new Icon_data({ iconUrl: "{{ asset('img/images/p8.png') }}" }),
    Icon_comercial = new Icon_data({ iconUrl: "{{ asset('img/images/p9.png') }}" }),
    Icon_taxi = new Icon_data({ iconUrl: "{{ asset('img/images/p10.png') }}" }),
    Icon_escolar = new Icon_data({ iconUrl: "{{ asset('img/images/p11.png') }}" }),
    Icon_metro = new Icon_data({ iconUrl: "{{ asset('img/images/p12.png') }}" }),
    Icon_trailer = new Icon_data({ iconUrl: "{{ asset('img/images/p13.png') }}" }),
    Icon_camion = new Icon_data({ iconUrl: "{{ asset('img/images/p14.png') }}" }),
    Icon_empresarial = new Icon_data({ iconUrl: "{{ asset('img/images/p15.png') }}" }),
    Icon_limite2 = new Icon_all({ iconUrl: "{{ asset('img/images/marker_data.png') }}" }),
    Icon_limite = new Icon_all({ iconUrl: "{{ asset('img/images/limite.png') }}" });
L.icon = function (options) { return new L.Icon(options); };

var LeafIcon = L.Icon.extend({
      options: {
        shadowUrl: "{{ asset('img/images/leaf-shadow.png') }}",
        iconSize: [38, 95],
        shadowSize: [50, 64],
        iconAnchor: [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor: [-3, -76]
        }
    });

var greenIcon = new LeafIcon({ iconUrl: "{{ asset('img/images/leaf-green.png') }}" }),
    redIcon = new LeafIcon({ iconUrl: "{{ asset('img/images/leaf-red.png') }}" }),
    orangeIcon = new LeafIcon({ iconUrl: "{{ asset('img/images/leaf-orange.png') }}" });

    /*
*Funcion que se encarga en la clasificacion de los vehiculos
*/
function capa_point(cordenada) {
    var arr_puntos0 = new Array();
    var arr_puntos1 = new Array();
    var arr_puntos2 = new Array();
    var arr_puntos3 = new Array();
    var arr_puntos4 = new Array();
    var arr_puntos5 = new Array();
    var arr_puntos6 = new Array();
    var arr_puntos7 = new Array();
    var layerControl = false;
    var count_cordenada = cordenada.length;
    //console.log(cordenada[0][0]);
    //console.log(cordenada[1][0]);
    //console.log(cordenada[2][0]);
    for (i = 0; i < count_cordenada; i++) {
        if (cordenada[i][0] == 1) {
            arr_puntos0.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_moto }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 2) {
            arr_puntos1.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_colectivo }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 3) {
            arr_puntos2.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_auto }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 4) {
            arr_puntos3.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_motoneta }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 5) {
            arr_puntos4.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_bicicleta }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 6) {
            arr_puntos5.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_taxi_informal }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 7) {
            arr_puntos6.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_camioneta }));
            cont_vehiculos++;
        }
        if (cordenada[i][0] == 8) {
            arr_puntos7.push(L.marker([cordenada[i][1], cordenada[i][2]], { icon: Icon_furgoneta }));
            cont_vehiculos++;
        }

    }
    if (layerControl === false) {
        layerControl = L.control.layers().addTo(map);
    }
    var puntos_mapa = L.layerGroup(null).addTo(map);
    if (arr_puntos0.length > 0) { var puntos_mapa0 = L.layerGroup(arr_puntos0).addTo(map); }
    else { var puntos_mapa0 = L.layerGroup(arr_puntos0); }
    if (arr_puntos1.length > 0) { var puntos_mapa1 = L.layerGroup(arr_puntos1).addTo(map); }
    else { var puntos_mapa1 = L.layerGroup(arr_puntos1); }
    if (arr_puntos2.length > 0) { var puntos_mapa2 = L.layerGroup(arr_puntos2).addTo(map); }
    else { var puntos_mapa2 = L.layerGroup(arr_puntos2); }
    if (arr_puntos3.length > 0) { var puntos_mapa3 = L.layerGroup(arr_puntos3).addTo(map); }
    else { var puntos_mapa3 = L.layerGroup(arr_puntos3); }
    if (arr_puntos4.length > 0) { var puntos_mapa4 = L.layerGroup(arr_puntos4).addTo(map); }
    else { var puntos_mapa4 = L.layerGroup(arr_puntos4); }
    if (arr_puntos5.length > 0) { var puntos_mapa5 = L.layerGroup(arr_puntos5).addTo(map); }
    else { var puntos_mapa5 = L.layerGroup(arr_puntos5); }
    if (arr_puntos6.length > 0) { var puntos_mapa6 = L.layerGroup(arr_puntos6).addTo(map); }
    else { var puntos_mapa6 = L.layerGroup(arr_puntos6); }
    if (arr_puntos7.length > 0) { var puntos_mapa7 = L.layerGroup(arr_puntos7).addTo(map); }
    else { var puntos_mapa7 = L.layerGroup(arr_puntos7); }

    layerControl.addBaseLayer(puntos_mapa, "Tipos de Vehículos")
        .addOverlay(puntos_mapa0, "Auto particular->" + arr_puntos0.length)
        .addOverlay(puntos_mapa1, "Buses-------------->" + arr_puntos1.length)
        .addOverlay(puntos_mapa2, "Taxi----------------->" + arr_puntos2.length)
        .addOverlay(puntos_mapa3, "Metrovía---------->" + arr_puntos3.length)
        .addOverlay(puntos_mapa4, "Moto--------------->" + arr_puntos4.length)
        .addOverlay(puntos_mapa5, "Camión----------->" + arr_puntos5.length)
        .addOverlay(puntos_mapa6, "Camioneta------->" + arr_puntos6.length)
        .addOverlay(puntos_mapa7, "Expreso---------->" + arr_puntos7.length);
    }

    function insertar_datos(coordenada, usuario) {
    jObject = JSON.stringify(coordenada);
    obj_us = JSON.stringify(usuario);
    $.ajax(
        {
            async: true,
            type: "POST",
            url: "../ajax_carga_data_insert",
            data: { 'jObject': jObject, 'obj_us': obj_us },
            beforeSend: function () {
                swal("Total de puntos: " + cont_vehiculos, "Ingresando los puntos a la base de datos.. Espere por favor...");
                console.log("cargando_insertar_datos...");
                document.getElementById('imagen').style.visibility = 'visible';
            },
            success: function (result) {
                document.getElementById('imagen').style.visibility = 'hidden';
                var JsonResult = result;
            },
            complete: function (result) {
                swal("", "Se cargaron los " + cont_vehiculos, "puntos exitosamente!", "success");
                bandera_historica = true;
            },
            error: function (result) {
                swal("", "Error en el ingreso de carga masiva de datos.!", "success", { icon: "warning", });
                //document.getElementById('imagen').style.visibility = 'hidden';
            }
        });
    }

    function ajax_r() {

    <?php  foreach($sectorp as $sector){  ?>
    <?php if($sector->estado == "A"){?>

    count_result = count_result_aux;
        coordenadas = new L.geoJson(<?= $sector->coordenadas;?>,{
            onEachFeature(feature, layer){
                layer.bindPopup("<?= $sector->sector;?>");
                coords.push(feature.geometry.coordinates);
                layerLatLong = layer.getLatLngs();
            }  
        });
        // map.fitBounds(coordenadas.getBounds());

        if (bandera_analisis == true) {
            var uidsector = <?= $sector->uidsector;?>;
            num_cluster = $('#num_cluster').val();
            $.ajax(
                {
                    type: "GET",
                    url: "../ajax_r_analisis/" + usuario + "/" + num_cluster + "/" + uidsector,
                    cache: false,
                    beforeSend: function () {
                        console.log(document.getElementById('imagen').style.visibility);
                        console.log("cargando...");
                        if (document.getElementById('imagen').style.visibility != 'hidden') {
                            var imagen = document.getElementById('imagen').src = "{{ asset('img/images/cargando_ajax.gif') }}";
                            console.log("ajax_r"+imagen);
                            document.getElementById('imagen').style.visibility = 'visible';
                        } else {
                            document.getElementById('imagen').style.visibility = 'visible';
                        }
                    },
                    success: function (result) {
                        if (result) {
                            //alert(result);
                            document.getElementById('imagen').style.visibility = 'visible';
                            document.getElementById("<?= $sector->uidsector;?>").style.visibility = 'visible';
                            swal("", "Se cargaron los datos exitosamente!", "success");
                            var imagen = document.getElementById('imagen').src = "{{ asset('img/images/logo.png') }}";
                            var imagen = document.getElementById("<?= $sector->uidsector;?>").src = "/img/images/"+"<?= $sector->uidsector;?>"+".png";
                            var imagen = document.getElementById("<?= $sector->uidsector;?>").alt="El sector <?= $sector->sector;?> no devolvio resultados...";
                            var cont_long = count_result / 2;
                            var res = result.split(",");
                            var count_result = res.length;
                            var count_result2 = count_result / 2;
                            var newcoorlatitud = new Array();
                            var newcoorlongitud = new Array();
                            for (j = 0; j < count_result; j++) {

                                res[j] = res[j].replace("[", "");
                                res[j] = res[j].replace("]", "");
                                res[j] = res[j].replace('"', "");
                                res[j] = res[j].replace('"', "");
                                res[j] = res[j].replace(" ", "");

                                //console.log(res[j]);
                                if (j < count_result2) {
                                    newcoorlatitud.push(res[j]);
                                } else {
                                    newcoorlongitud.push(res[j]);
                                }
                            }
                            // console.log(newcoorlatitud);
                            // console.log(newcoorlongitud);
                            latitud = newcoorlatitud;
                            longitud = newcoorlongitud;


                            for (i = 0; i < count_result2; i++) {

                                
                                L.icon = function (options) {
                                    return new L.Icon(options);
                                };

                                L.marker([newcoorlatitud[i], newcoorlongitud[i]], { icon: greenIcon }).addTo(map)
                                    .bindPopup('Congestionamiento<br> Vehicular.<br>Latitud: '+newcoorlatitud[i]+'<br>Longitud: '+newcoorlongitud[i])
                                    .openPopup();

                            }

                        }
                        else {
                            swal("", "Error al generar el análisis.!", "success", { icon: "warning", });
                            document.getElementById('imagen').style.visibility = 'hidden';
                        }
                    },
                    error: function (result) {
                        swal("", "Error al generar el análisis.!", "success", { icon: "warning", });
                        document.getElementById('imagen').style.visibility = 'hidden';
                    }
                });
        }
    <?php } ?>
    <?php } ?>



    else {
        swal("", "Por favor primero, indique la zona donde desea realizar el analisis!", "success", { icon: "warning", });
    }
    }

    </script>
   
  </div>
</div>

<!-- <div class="col-md-8">
    <div class="x_panel">
    
    </div>
</div> -->

<div class="col-md-8">
    <div class="x_panel">
                    <!--PANEL QUE MUESTRA LA IMAGEN PNG DEL ANALSIS POR CADA SECTOR, EN CASO QUE NO SE DEVUELVA UNA IMAGEN NOS MUESTRA UN MENSAJE INDICANDO QUE NO EXISTEN RESULTADOS...-->

    <?php  foreach($sectorp as $sector){  ?>
        <?php if($sector->estado == "A"){?>
            <div class="panel panel-primary">
            <div class="panel-heading"><?= $sector->sector;?></div>
            <img id="<?= $sector->uidsector;?>" class="img-responsive" style="height: 450px;visibility: hidden;">
            <h3 style="text-align:center"><?= $sector->sector;?></h3>
            </div> 
        <?php } ?>
    <?php } ?>
    </div>
</div>   
    

</body>

@endsection