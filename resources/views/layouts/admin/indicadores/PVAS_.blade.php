@extends('layouts.admin.base')

@section('title', 'Indicadores')

@section('content')
<?php
$conexioncon="host='127.0.0.1' port='5432' dbname='datos_gye' user='postgres' password='12345'";
$dbconn=pg_connect($conexioncon)or die('no se pudo conectar a la base de datos:'.pg_last_error());
?>

<link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Analizar Indicadores:<small></small></h3>
      </div>
   </div>
   </div>
   <br />
   <br />
     <section class="portafolio" style="padding-top:15px;">
		<h2  style="text-align:center">PORCENTAJE DE VEHICULOS AL AÑO POR SECTOR</h2>
        <form action="{{ url('admin/indicadores/PVAS') }}" method="post">
        <label>Desde:</label><input type="date"  value="fechaI" />
        <label>Hasta:</label><input type="date"  value="fechaF" />
        <!--<label>Año:</label>
        <select>
        	<option>======</option>
            <option>2017</option>
            <option>2018</option>
            <option>2019</option>
        </select>
        
        <label>Mes:</label>
        <select>
        	<option>======</option>
            <option>Enero</option>
            <option>Febrero</option>
            <option>Marzo</option>
            <option>Abril</option>
            <option>Mayo</option>
            <option>Junio</option>
            <option>Julio</option>
            <option>Agosto</option>
            <option>Septiembre</option>
            <option>Octubre</option>
            <option>Noviembre</option>
            <option>Diciembre</option>
        </select>-->
        <input type="submit" value="PRUEBA" />
        
        </form>
        <?php
		
		?>
        <?php 
		if(isset($_POST['PRUEBA'])){
		$fechaI = DateTime::createFromFormat('Y-m-d', $_POST['fechaI']);
		$fechaD = $fechaI->format('Ymd');
		$fechaF = DateTime::createFromFormat('Y-m-d', $_POST['fechaF']);
		$fechaH = $fechaF->format('Ymd');
		
		$sqlcon =pg_query("Select 
x.sector, x.total ,  round(((x.total*100)::numeric/(

Select sum(x.total) total from (  SELECT b.sector, count( distinct a.id_trayectoria) as  total
FROM public.trayectoria_gyes_detalle a,
public.ge_sectores b
where b.uidsector=a.id_sector and b.estado='A' and a.fecha between '".$fechaD."' and '".$fechaH."'
group by b.sector) x
) ),4) as porcentaje
from (  SELECT b.sector, count( distinct a.id_trayectoria) as  total
FROM public.trayectoria_gyes_detalle a,
public.ge_sectores b
where b.uidsector=a.id_sector and b.estado='A' and a.fecha between '".$fechaD."' and '".$fechaH."'
group by b.sector) x");
		$nfilascon= pg_num_rows($sqlcon);
		
		
		
		
		}
		?>
      
         <script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
  <script src="{{ asset('vendors/highgraphics/code/graficas.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/data.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/drilldown.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="container" style="min-width: 210px; height: 300px; margin: 0 auto"></div>


		<script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
<!--var options={-->
    chart: {
        type: 'column'
    },
    title: {
        text: 'Browser market shares. January, 2018'
    },
    subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total percent market share'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: 'Browsers',
            colorByPoint: true,
            data: [
                {
                    name: 'Chrome',
                    y: 12,
                    drilldown: 'Chrome'
                },
                {
                    name: 'Firefox',
                    y: 10.57,
                    drilldown: 'Firefox'
                },
                {
                    name: 'Internet Explorer',
                    y: 7.23,
                    drilldown: 'Internet Explorer'
                },
                {
                    name: 'Safari',
                    y: 5.58,
                    drilldown: 'Safari'
                },
                {
                    name: 'Edge',
                    y: 4.02,
                    drilldown: 'Edge'
                },
                {
                    name: 'Opera',
                    y: 1.92,
                    drilldown: 'Opera'
                },
                {
                    name: 'Other',
                    y: 7.62,
                    drilldown: null
                }
            ]
		}
	],
    drilldown: {
        series: [
            {
                name: 'Chrome',
                id: 'Chrome',
                data: [
                    [
                        'v65.0',
                        0.1
                    ],
                    [
                        'v64.0',
                        1.3
                    ],
                    [
                        'v63.0',
                        53.02
                    ],
                    [
                        'v62.0',
                        1.4
                    ],
                    [
                        'v61.0',
                        0.88
                    ],
                    [
                        'v60.0',
                        0.56
                    ],
                    [
                        'v59.0',
                        0.45
                    ],
                    [
                        'v58.0',
                        0.49
                    ],
                    [
                        'v57.0',
                        0.32
                    ],
                    [
                        'v56.0',
                        0.29
                    ],
                    [
                        "v55.0",
                        0.79
                    ],
                    [
                        'v54.0',
                        0.18
                    ],
                    [
                        'v51.0',
                        0.13
                    ],
                    [
                        'v49.0',
                        2.16
                    ],
                    [
                        'v48.0',
                        0.13
                    ],
                    [
                        'v47.0',
                        0.11
                    ],
                    [
                        'v43.0',
                        0.17
                    ],
                    [
                        'v29.0',
                        0.26
                    ]
                ]
            },
            {
                name: 'Firefox',
                id: 'Firefox',
                data: [
                    [
                        'v58.0',
                        1.02
                    ],
                    [
                        'v57.0',
                        7.36
                    ],
                    [
                        'v56.0',
                        0.35
                    ],
                    [
                        'v55.0',
                        0.11
                    ],
                    [
                        'v54.0',
                        0.1
                    ],
                    [
                        'v52.0',
                        0.95
                    ],
                    [
                        'v51.0',
                        0.15
                    ],
                    [
                        'v50.0',
                        0.1
                    ],
                    [
                        'v48.0',
                        0.31
                    ],
                    [
                        'v47.0',
                        0.12
                    ]
                ]
            },
            {
                name: 'Internet Explorer',
                id: 'Internet Explorer',
                data: [
                    [
                        'v11.0',
                        6.2
                    ],
                    [
                        'v10.0',
                        0.29
                    ],
                    [
                        'v9.0',
                        0.27
                    ],
                    [
                        'v8.0',
                        0.47
                    ]
                ]
            },
            {
                name: 'Safari',
                id: 'Safari',
                data: [
                    [
                        'v11.0',
                        3.39
                    ],
                    [
                        'v10.1',
                        0.96
                    ],
                    [
                        'v10.0',
                        0.36
                    ],
                    [
                        'v9.1',
                        0.54
                    ],
                    [
                        'v9.0',
                        0.13
                    ],
                    [
                        'v5.1',
                        0.2
                    ]
                ]
            },
            {
                name: 'Edge',
                id: 'Edge',
                data: [
                    [
                        'v16',
                        2.6
                    ],
                    [
                        'v15',
                        0.92
                    ],
                    [
                        'v14',
                        0.4
                    ],
                    [
                        'v13',
                        0.1
                    ]
                ]
            },
            {
                name: 'Opera',
                id: 'Opera',
                data: [
                    [
                        'v50.0',
                        0.96
                    ],
                    [
                        'v49.0',
                        0.82
                    ],
                    [
                        'v12.1',
                        0.14
                    ]
                ]
            }
        ]
    }
});
		</script>
        =============================================================================================================================================
        <form action="{{ url('admin/indicadores') }}">
        <input type="submit" value="Regresar"/>
        </form>
                </section>
             

@endsection