@extends('layouts.admin.base')

@section('title', 'Indicadores')

@section('content')


<link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
<script>
function startDownloadMAWS()  
{  
     var url='http://54.207.52.86/packages/archivos/MANUALES.pdf';    
     window.open(url, 'Download');  
     //http://54.207.52.86 => Producción
	 //http://127.0.0.1:8000 => Local
}
</script>
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Analizar Indicadores:<small></small></h3>
      </div>
   </div>
   </div>
   <br />
   <br />
   <section class="portafolio">
   <div class="portafolio-container">
   <section class="portafolio-item">
   				<a href="{{ url('admin/indicadores/PVAS') }}" style="color:#000"><img src="{{asset('packages/images/modulos/PVAS.gif')}}" class="portafolio-img" title="PVAS"></a>
                <section class="portafolio-text">
                <p style="font-size:10px">Este indicador se encargará de medir las cantidades y porcentajes de los tipos de transportes en los
                diferentes sectores existentes </p>
                </section></a>
                </section>
                <section class="portafolio-item">
   				<a href="{{ url('admin/indicadores/VPC') }}" style="color:#000"><img src="{{asset('packages/images/modulos/VPC.gif')}}" class="portafolio-img" title="VPC"></a>
                <section class="portafolio-text">
                <p style="font-size:10px;">Este indicador medirá la cantidad de motos y autos particulares en los respectivos años</p>
                </section></a>
                </section>
                <section class="portafolio-item">
   				<a href="{{ url('admin/indicadores/twitter') }}" style="color:#000"><img src="{{asset('packages/images/modulos/TWEETS.gif')}}" class="portafolio-img" title="TWEETS">
                <section class="portafolio-text">
                <p style="font-size:10px">Este indicador se encargará de medir los reportes de choques, embotellamientos y accidentes de los Twits guardados en las bases de datos, con su respectiva fecha(Diaria, Mensual o Anual)</p>
                </section></a>
                </section>
                </div>
                </section>
                <section class="portafolio" style="margin-top:15px;">
   <div class="portafolio-container">
   <section class="portafolio-item">
   				<a href="{{ url('admin/indicadores/VPT') }}" style="color:#000"><img src="{{asset('packages/images/modulos/VPT.gif')}}" class="portafolio-img" title="VPT">
                <section class="portafolio-text">
                <p style="font-size:10px">Este indicador se encargará de medir las velocidades de los vehiculos de una fecha en el sector</p>
                </section></a>
                </section>
                <section class="portafolio-item">
   				<a href="{{ url('admin/indicadores/FHP') }}" style="color:#000"><img src="{{asset('packages/images/modulos/FHP.gif')}}" class="portafolio-img" title="FHP">
                <section class="portafolio-text">
                <p style="font-size:10px;">Este indicador se encargará de medir el volumen de vehiculos en una hora determinada en el sector elegido</p>
                </section></a>
                </section>
                <section class="portafolio-item">
   				<a href="#" onclick="startDownloadMAWS();"style="color:#000"><img src="{{asset('packages/images/modulos/help.jpg')}}" class="portafolio-img" title="GUÍA"></a>
                <section class="portafolio-text">
                <h2>Guía</h2>
                <p>Manual Técnico y de Usuario</p>
                </section></a>
                </section>
                </div>
                </section>
             

@endsection