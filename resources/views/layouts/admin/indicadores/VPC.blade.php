@extends('layouts.admin.base')

@section('title', 'Indicadores')

@section('content')


<link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Analizar Indicadores:<small></small></h3>
      </div>
   </div>
   </div>
  <script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/series-label.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/exporting.js') }}"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>



		<script type="text/javascript">

Highcharts.chart('container', {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: ['2016', '2017', '2018', '2019']
    },
    labels: {
        items: [{
            html: 'Total de Vehiculos Privados en Circulación',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Auto Particular',
        data: [3, 2, 1, 3]
    }, {
        type: 'column',
        name: 'Motocicleta',
        data: [2, 3, 5, 7]
    }, {
        type: 'spline',
        name: 'Average',
        data: [3, 2.67, 3, 6.33],
        marker: {
            lineWidth: 8,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'pie',
        name: 'Total',
        data: [{
            name: 'Auto Particular',
            y: 13,
            color: Highcharts.getOptions().colors[4] // Jane's color
        }, {
            name: 'Motocicleta',
            y: 23,
            color: Highcharts.getOptions().colors[5] // John's color
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});


		</script>

@endsection