@extends('layouts.admin.base')

@section('title', 'Home')

@section('content')

<script type="text/javascript">  
function startDownloadMAWS()  
{  
     var url='http://54.207.52.86/packages/archivos/MUAWS.pdf';    
     window.open(url, 'Download');  
     //http://54.207.52.86
}
</script>

<link href="http://127.0.0.1:8000/packages/css/estilos.css" rel="stylesheet">

<h1>Procedimientos Críticos en AWS</h1>
<br><br>

<button class="accordion">Procedimiento para migrar instancias a otra cuenta</button>
<div class="panel">
  <p>1. Crear una cuenta en AWS</p>
  <p>2. Crear imágenes de las instancias a migrar</p>
  <p>3. Identificar el número de la cuenta nueva</p>
  <p>4. Editar los permisos de la instancia a migrar</p>
  <p>5. Compartir la imagen con otra cuenta</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<button class="accordion">Procedimiento para Expandir el tamaño de los volumenes</button>
<div class="panel">
  <p>1. Identificar el disco a expandir</p>
  <p>2. Modificar el volumen al tamaño deseado</p>
  <p>3. Verificar en la consola de la instancia que el tamaño se expandió con el comando lsblk</p>
  <p>4. Reiniciar la instancia de no haberse aplicado el cambio</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>


<button class="accordion">Procedimiento para recuperar acceso ssh a instancia cuando se pierde la clave pública</button>
<div class="panel">
  <p>1. Crear una imágen de la instancia</p>
  <p>2. Levantar instancia basada en una imágen</p>
  <p>3. Asignar una clave pública (nueva o antigua)</p>
  <p>4. Ingresar por un programa ssh para comprobar conexión</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<button class="accordion">Procedimiento para crear una clave pública basada en archivo .pem</button>
<div class="panel">
  <p>1. Crear una key pair y descargar el archivo en un directorio local</p>
  <p>2. Abrir el programa putty gen</p>
  <p>3. Cargar el archivo, seleccionar la opción SSH-1(RSA)</p>
  <p>4. Descargar el archivo en un directorio local seguro</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<button class="accordion">Procedimiento para ingresar por putty a una instancia</button>
<div class="panel">
  <p>1. Identificar la información para conexión a la instancia </p>
  <p>2. Conectarse por putty</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<button class="accordion">Procedimiento para generar los billing alerts</button>
<div class="panel">
  <p>1. Procedimiento para generar los billing alerts</p>
  <p>2. Click en Budgets</p>
  <p>3. Create budgets</p>
  <p>4. Completar la información solicitada</p>
  <p>5. Verificar y aceptar la creación</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<button class="accordion">Procedimiento para utilizar la Calculadora de AWS</button>
<div class="panel">
  <p>1.	Acceder a la página https://calculator.aws/#/ </p>
  <p>2.	Crear estimado, asignar un nombre y seleccionar una región</p>
  <p>3.	Agregar servicio</p>
  <p>4.	Ingresar la información de acuerdo con el servicio que se desea estimar</p>
  <p>5.	Revisar los estimados que indica la calculadora</p>
  <p><a href="#" onclick="startDownloadMAWS();">VER MANUAL</a></p>
</div>

<style>
.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
</style>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

 @endsection