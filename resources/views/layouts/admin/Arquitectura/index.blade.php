@extends('layouts.admin.base')

@section('title', 'Home')

@section('content')


<link href="{{asset('packages/css/estilos.css')}}" rel="stylesheet">

<h1>Arquitectura LessTraffic</h1>


<div id="the-slider" class="carousel slide" data-ride="carousel">
	
	<ol class="carousel-indicators">
    @foreach($modelo as $item)
        <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}"
            class="{{ $loop->first ? ' active' : '' }}"></li>
    @endforeach
	</ol>

    <div class="carousel-inner">
	
    @foreach($modelo as $item)
    	<div class="item {{ $loop->first ? ' active' : '' }}">
			<img src="{{$item->detalle_arq_comp}}" class="img-responsive" alt="...">
			<div class="carousel-caption">	
				<div class="agileits-banner-info">
					<h3>{{$item->nombre}}</h3>
					<div class="more">
					@php
					$id=($item->id_modulos);
					@endphp
						<a href="{{url('admin/Arquitectura/Formulario', $id)}}" class="btn btn-info btn-xs" role="button">Leer más... </a>
					</div>
				</div>
      		</div>
		</div>
	@endforeach
	</div> <!--carousel-inner-->

	<!--controls-->
	<a class="left carousel-control" href="#the-slider" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#the-slider" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
  	
</div>

<!-- <img src="http://127.0.0.1:8000/packages/images/modulos/arq.jpg" class="img-responsive" alt="..."> -->
<!-- href="{{asset('packages/css/estilos.css')}}" -->
<img src="{{asset('packages/images/modulos/arq.jpg')}}" class="img-responsive" alt="...">

 @endsection