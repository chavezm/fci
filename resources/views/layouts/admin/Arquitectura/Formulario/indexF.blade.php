@extends('layouts.admin.base')

@section('title', 'Home')

@section('content')

<style>
th{
		font-weight:800;
		font-size:20px;
		}    
</style>

<h1>Arquitectura LessTraffic</h1>



@foreach($modelo as $item)

	   
    <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                    <div class="panel-body">
                        <table id="mytable" class="table table-bordred table-striped" WIDTH="100%">
                            <thead>
                            <th>ARQUITECTURA: </th> 
                            <th>{{$item->nombre}}</th>                   
                            </thead>
                            <tbody>                       
                            @foreach($modelo as $item ) 
                            <tr>
                            <td WIDTH=70%><div class="item {{ $loop->first ? ' active' : '' }}">
		                            <img src="{{$item->detalle_arq_comp}}" class="img-responsive" alt="..." WIDTH=100%>
                                </div>
                            </td>
                            <td align="justify">{{$item->descripcion}}</td>
                            </tr>
                            
                            <tr>
                                <td>INSTANCIA CODIGO: </td>
                                <td>{{$item->detalle_instancias}}</td>
                            </tr>

                            <tr>
                                <td>INSTANCIA CODIGO AWS: </td>
                                <td>{{$item->codigoAWS}}</td>
                            </tr>
                            
                            <tr>
                                <td>INSTANCIA IP: </td>
                                <td>{{$item->ip_publica}}</td>
                            </tr>

                            <tr>
                                <td>VOLUMEN CODIGO: </td>
                                <td>{{$item->detalle_volumenes}}</td>
                            </tr>

                            <tr>
                                <td>VOLUMEN CODIGO AWS: </td>
                                <td>{{$item->codigoAWS_V}}</td>
                            </tr>

                            <tr>
                                <td>VOLUMEN CAPACIDAD: </td>
                                <td>{{$item->capacidad}}</td>
                            </tr>

                            <tr>
                                <td>MANUAL DE USUARIO: </td>
                                <td><a href="{{$item->url_manual_u}}" >VER MANUAL!</a></td>
                            </tr>

                            <tr>
                                <td>MANUAL DE TÉCNICO: </td>
                                <td><a href="{{$item->url_manual_t}}" >VER MANUAL!</a></td>
                            </tr>

                            <tr>
                                <td>LINK DEL MÓDULO: </td>
                                <td><a href="{{$item->observacion}}">IR AL MÓDULO</a></td>
                            </tr>

                            @endforeach                      
                        </tbody>
                        </table>
                    </div>
            </div>       
        </div>

@endforeach

@endsection