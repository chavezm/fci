@extends('layouts.admin.base')

@section('title', 'Indicadores')

@push('styles2')
  <!-- <link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet"> -->
@endpush

@section('content')
<div id="vue">
	<div class="">
	    <div class="page-title">
	      <div style="text-align:center">
             <center><h2><b><a style="font-size:12px; margin-right:100px;"  href="{{ url('admin/indicadores/twitter') }}">Anterior Indicador</a><b><b>VELOCIDAD PROMEDIO DE TIPOS DE VEHÍCULOS POR SECTOR<b><b><a style="font-size:12px; margin-left:180px;"  href="{{ url('admin/indicadores/FHP') }}">Siguiente Indicador >></a><b></h2><center>
            
	      </div>
	   </div>
   </div>
   <br />
   <br />
            <label style="float:left; font-size:20px;">Fecha Inicio:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_inicial" required>
       <label style="float:left; font-size:20px; margin-left:10px">Fecha Final:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_final" required>
           <label style="float:left; font-size:20px; margin-left:10px">Tipo de Vehículo:</label>
            <select class="form-control" v-model="tipovehiculo_seleccionado" style="width:155px; float:left; margin-left:8px">
                <option value="0" disabled>Seleccione...</option>
                <option v-for="tipovehiculo in tipovehiculos" v-bind:value="tipovehiculo">
                    @{{ tipovehiculo.nombre }}
                </option>
            </select>
            <button @click ="btn_graficar()" class="btn btn-primary btn-sm" style="margin-left:16px"><i class="fa fa-bar-chart"></i> Graficar</button>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div id="mapa" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
        </div>
        <div class="col-md-6 col-sm-12">
            <!-- <pre>@{{ $data }}</pre> -->
            <div id="container" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
        </div>
    </div>
</div>
  

@endsection

@push('script')
	<script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/timeline.js ') }}"></script>
  <script src="{{ asset('js/lodash.min.js') }}"></script>
	<script src="{{ asset('js/vue.js') }}"></script>
	<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
  <!-- mapa -->
    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>
    <script src="{{ asset('js/mapa.js') }}"></script>
  <script>
    var app = new  Vue({
        el:"#vue",
        data:{
            series_data:[],
            series_data_grapic:[],
            tipovehiculos:[],
            tipovehiculo_seleccionado:0,
            fecha_inicial:null,
            fecha_final:null,
        },
        created:function(){
            this.listarTipoVehiculo();
        },
    methods:{
        graficar:function(){
            graficar_event();
            dibujar(app.series_data); // MAPA
        },
        listarTipoVehiculo:function(){
            $.post('/admin/indicadores/VPT-tipo-vehiculo',{},function(response){
                if(!response.existeError){
                    app.tipovehiculos = response.data;
                }
            },'json');
        },
        btn_graficar(){
            if(_.isEmpty(app.fecha_inicial) || _.isEmpty(app.fecha_final)){
                Swal.fire('','Los campos de fechas no deben estar vacios','error');
            }else{
                if(_.isEqual(app.tipovehiculo_seleccionado,0)){
                    Swal.fire('','Debe seleccionar al menos un tipo de vehiculo','error');
                }else{
                    $.post('/admin/indicadores/VPT',{
                      fecha_inicial: app.fecha_inicial,
                      fecha_final: app.fecha_final,
                      tipo_vehiculo: app.tipovehiculo_seleccionado.id_vehiculo
                    },function(response){
                        if(!response.existeError){
                            app.series_data = response.data;
                            app.series_data_grapic = [];
                            $.each(app.series_data, function(i, item) {
                                app.series_data_grapic.push({
                                    'name'      : item.año + ' ' + item.nombre + ' ' + item.sector,
                                    'y'         : (item.velocidad * 1)
                                });
                            });
                            app.graficar();
                            // Validar Recomendacion
                            var obj = _.minBy(app.series_data,function(o){ return _.floor(o.velocidad,2); });
							if(obj.velocidad < 20){
                            recomendacion('Debido que en el sector ' + obj.sector + ' registra una velocidad ' + _.floor(obj.velocidad,2)+' km/h menor a la indicada(20km/h), se sugiere un mayor personal de agentes de tránsito para la supervisión de velocidades mínimas del tipo de transporte ' + app.tipovehiculo_seleccionado.nombre + ' que puedan producir tráfico en el sector','info');
							}
                        }else{
                            Swal.fire('','No existen Registros para mostrar','warning');
                        }
                    },'json');
                }
            }
        },
      },
      watch:{

      }
    });

    function graficar_event(){
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'VELOCIDAD PROMEDIO DE LOS TIPOS DE VEHÍCULOS P0R SECTOR'
            },
            subtitle: {
                text: '<b>Desde: ' + app.fecha_inicial + '  <br><b>Hasta: ' + app.fecha_final + '  <br><b>Tipo de Vehiculo: ' + app.tipovehiculo_seleccionado.nombre 
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Velocidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.2f} km/h'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> km/h<br/>'
            },

            series: [
                {
                    name: "Datos",
                    colorByPoint: true,
                    data: app.series_data_grapic
                }
            ]
        });
        $('.highcharts-credits').remove();
    }
  </script>
@endpush