@extends('layouts.admin.base')

@section('title', 'Indicadores')

@push('styles2')
	<link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
@endpush

@section('content')
<div id="vue">
	<div class="">
	    <div class="page-title">
	      <div style="text-align:center">
             <center><h2><b><a style="font-size:12px; margin-right:270px;"  href="{{ url('admin/indicadores/PVAS') }}">Anterior Indicador</a><b><b>VEHÍCULOS PRIVADOS EN CIRCULACIÓN<b><b><a style="font-size:12px; margin-left:210px;"  href="{{ url('admin/indicadores/twitter') }}">Siguiente Indicador >></a><b></h2><center>
            
	      </div>
	   </div>
   </div>
   <br />
   <br />
       <label style="float:left; font-size:20px;">Fecha Inicio:</label> <input type="date" min="2018-09-02"class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_inicial" required>
       <label style="float:left; font-size:20px; margin-left:10px">Fecha Final:</label> <input type="date"  max="2019-09-30" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_final" required>
	   <button @click ="btn_graficar()" class="btn btn-primary" style="margin-left:16px"><i class="fa fa-bar-chart"></i> Graficar</button>
    <br />
    <br />
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div id="mapa" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
		</div>
        <br />
    	<div class="col-md-6 col-sm-12">
			<!-- <pre>@{{ $data }}</pre> -->
			<div id="container" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
		</div>
	</div>
</div>
	

@endsection

@push('script')
	<script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
	<script src="{{ asset('vendors/highgraphics/code/modules/data.js') }}"></script>
	<script src="{{ asset('vendors/highgraphics/code/modules/drilldown.js') }}"></script>
	<script src="{{ asset('js/lodash.min.js') }}"></script>
	<script src="{{ asset('js/vue.js') }}"></script>
	<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
	<!-- mapa -->
	<script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>
	<script src="{{ asset('js/mapa.js') }}"></script>
	<script>
		var app = new  Vue({
			el:"#vue",
			data:{
				vehiculos_en_circulacion:[],
				series_data:[],
				drilldown_data:[],
				fecha_inicial:null,
				fecha_final:null,
				total_pastel:0
			},
			created:function(){
				
			},
			methods:{
				graficar:function(){
					graficar_event();
					dibujar(app.vehiculos_en_circulacion); // MAPA
				},
				getDetalle(anio){
					$.post('/admin/indicadores/VPC/detalle/'+anio,function(response){
						var detalle_ = [];
						$.each(response, function(i, item) {
							var a = [ item.nombre,(item.vehiculos_privados *1) ];
							detalle_.push(a);
						});
						app.drilldown_data.push({
					    	'name':anio,
					    	'id':anio,
					    	'data': detalle_
					    });
					},'json');
				},
				btn_graficar(){
					if(_.isEmpty(app.fecha_inicial) || _.isEmpty(app.fecha_final)){
			            Swal.fire('','Los campos de fechas no deben estar vacios','error');
			        }else{
						$.post('/admin/indicadores/VPC',{
							fecha_inicial: app.fecha_inicial,
							fecha_final: app.fecha_final
						},function(response){
							if(!response.existeError){
								app.vehiculos_en_circulacion = response.data;
								app.series_data = [];
								app.total_pastel = 0;
								$.each(response.data, function(i, item) {
									app.total_pastel = app.total_pastel + (item.vehiculos_privados * 1);
								    app.series_data.push({
								    	'name'		: 'Año:' + item.año,
								    	'y'			: (item.vehiculos_privados * 1),
								    	'drilldown' : item.año
								    });
								    app.getDetalle(item.año);
								});
								//app.series_data.push({'name':'Nones','y':app.total_pastel,'drilldown':'Nones'});
								app.graficar();
								// VALIDACION DE RECOMENDACION
								var fecha = new Date();
								var anio_actual = fecha.getFullYear();
								var cantidad_actual=0,cantidad_anterior=0;
								$.each(app.vehiculos_en_circulacion, function(i, item) {
									if(_.isEqual(anio_actual,_.toInteger(item.año))){
										cantidad_actual += item.vehiculos_privados;
									}else //if(_.isEqual((anio_actual - 1),item.año)){
										cantidad_anterior += item.vehiculos_privados;
									//}
								});
								console.log(cantidad_actual);
								console.log(cantidad_anterior);
								if(cantidad_actual > cantidad_anterior){
									recomendacion("Para la disminución de la cantidad de vehículos privados en circulación se sugiere mejorar la comodidad del transporte pública, ya que en el presente año se reporta una mayor de "+cantidad_actual+" vehículos a comparación de los años anteriores",'info');
								}else{
									recomendacion("Según la cantidad de "+cantidad_anterior+" vehículos en el presente año se sugiere seguir promocionando la comodidad y calidad del transporte público",'info');
								}
								///
							}else{
								Swal.fire('','No existen Registros para mostrar','warning');
							}
						},'json');
					}
				},
				findDrilldown(id){
					var r=null;
					$.each(app.drilldown_data, function(i, item) {
						if(item.id == id){
							r = item;
						}
					});
					return r;
				}
			},
			watch:{

			},
			computed:{

			}
		});

	function graficar_event(){
		// Create the chart
			Highcharts.chart('container', {
			    chart: {
			        type: 'pie',
			        events: {
			        	click:function(e){
			        		dibujar(app.vehiculos_en_circulacion); // MAPA
			        	},
			            drilldown: function (e) {
			                if (!e.seriesOptions) {

			                    var chart = this,
			                        series = [];

			                        chart.showLoading('Cargando Vehículos Privados en Circulación...');

			                        $.post('/admin/indicadores/VPC/detalle/'+e.point.name.split(":")[1],function(response){
										var detalle_ = [];
										$.each(response, function(i, item) {
											var a = [ item.nombre,(item.vehiculos_privados *1) ];
											detalle_.push(a);
										});
										series.push({
									    	'name':e.point.name.split(":")[1],
									    	'id':e.point.name.split(":")[1],
									    	'data': detalle_
									    });

									    console.log(series[0]);

									    chart.hideLoading();
			                        	chart.addSeriesAsDrilldown(e.point, series[0]);

			                        	dibujar(response); // MAPA
									},'json');
			                }
			            }
			        }
			    },
			    title: {
			        text: 'VEHÍCULOS PRIVADOS EN CIRCULACIÓN'
			    },
			    subtitle: {
			        text: '<b>Desde: ' + app.fecha_inicial + '  <br><b>Hasta: ' + app.fecha_final 
			    },
			    xAxis: {
			        type: 'category'
			    },
			    yAxis: {
			        title: {
			            text: 'Total percent market share'
			        }
			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            // borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.name} | Total: {point.y:.1f}'
			            }
			        }
			    },

			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> veh<br/>'
			    },

			    series: [
			        {
			            name: "Datos",
			            colorByPoint: true,
			            data: app.series_data
			        }
			    ],
			    drilldown: {
			        series: []//app.drilldown_data
			    }
			});
			$('.highcharts-credits').remove();
		}
	</script>
@endpush