@extends('layouts.admin.base')

@section('title', 'Indicadores')

@push('styles2')
  <!-- <link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet"> -->
  <script>
function startDownloadMAWS()  
{  
     var url='http://54.207.52.86/packages/archivos/MANUALES.pdf';    
     window.open(url, 'Download');  
     //http://54.207.52.86 = Producción
	 //http://127.0.0.1:8000 => Local
}
</script>
@endpush

@section('content')
<div id="vue">
	<div class="">
	    <div class="page-title">
	      <div style="text-align:center">
             <center><h2><b><a style="font-size:12px; margin-right:300px;"  href="{{ url('admin/indicadores/VPT') }}">Anterior Indicador</a><b><b>FACTOR HORA PICO<b><b><a style="font-size:12px; margin-left:380px;"  href="#" onclick="startDownloadMAWS();">Manuales</a><b></h2><center>
            
	      </div>
	   </div>
   </div>
   <br />
   <br/>
   <div class="form-group form-inline text-center col-md-12">
        <label style="font-size:20px; margin-left:10px">Sectores:</label>
        <select class="form-control" v-model="sector_seleccionado" style="width:205px;">
            <option value="0" disabled>Seleccione...</option>
            <option v-for="sector in sectors" v-bind:value="sector">
                @{{ sector.sector }}
            </option>
        </select>
        <button @click ="btn_graficar()" class="btn btn-primary btn-sm" style="margin-left:16px"><i class="fa fa-bar-chart"></i> Graficar</button>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div id="mapa" style="min-width: 210px; height: 600px; margin: 0 auto"></div>
        </div>
        <div class="col-md-6 col-sm-12">
            <!-- <pre>@{{ $data }}</pre> -->
            <div id="container" style="min-width: 210px; height: 600px; margin: 0 auto"></div></div>
        </div>
    </div>
</div>
@endsection

@push('script')
	<script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
<script src="{{ asset('vendors/highgraphics/code/modules/timeline.js ') }}"></script>
  <script src="{{ asset('js/lodash.min.js') }}"></script>
  <script src="{{ asset('js/vue.js') }}"></script>
  <script src="{{ asset('js/sweetalert2@8.js') }}"></script>
  <!-- mapa -->
    <script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>
    <script src="{{ asset('js/mapa.js') }}"></script>
  <script>
    var app = new  Vue({
      el:"#vue",
      data:{
        series_data:[],
        sectors:[],
        series_data_grapic:[],
        categories_data_grapic:[],
        sector_seleccionado:0,
        fecha_inicial:null,
        fecha_final:null,
      },
    created:function(){
        this.listarSectores();
    },
    methods:{
        graficar:function(){
            graficar_event();
            dibujar(app.series_data); // MAPA
        },
        listarSectores:function(){
            $.post('/admin/indicadores/FHPSector',{},function(response){
                if(!response.existeError){
                    app.sectors = response.data;
                }
            },'json');
        },
        btn_graficar(){
            if(_.isEqual(app.sector_seleccionado,0)){
                Swal.fire('','Debe seleccionar al menos un Sector','error');
            }else{
                $.post('/admin/indicadores/FHP',{
                  sector: app.sector_seleccionado.uidsector,
                },function(response){
                    if(!response.existeError){
                        app.series_data = response.data;
                        app.series_data_grapic = [];
                        app.categories_data_grapic = [];
                        $.each(app.series_data, function(i, item) {
                            app.series_data_grapic.push( item.cant );
                            app.categories_data_grapic.push( 'Vehiculo: ' + item.nombre + ' | Hora: ' + item.hora );
                        });
                        app.graficar();
                        // Validacion de Recomendacion
                        var obj = app.series_data.sort(function (a, b) { return b.cant - a.cant; }).slice(0, 3);
                        if(obj.length == 3){
                            recomendacion("En las horas picos "+obj[0].hora+", "+obj[1].hora+" y "+obj[2].hora+", se recomienda mejorar la sincronización de los semáforos. Además de implementar tecnología para que los semáforos puedan ser operados a distancia",'info');
                        }else if(obj.length == 2){
                            recomendacion("En las horas picos "+obj[0].hora+" y "+obj[1].hora+", se recomienda mejorar la sincronización de los semáforos. Además de implementar tecnología para que los semáforos puedan ser operados a distancia",'info');
                        }else if(obj.length == 1){
                            recomendacion("En las horas picos "+obj[0].hora+", se recomienda mejorar la sincronización de los semáforos. Además de implementar tecnología para que los semáforos puedan ser operados a distancia",'info');
                        }
                    }else{
                        Swal.fire('','No existen Registros para mostrar','warning');
                    }
                },'json');
            }
        },
      },
      watch:{

      }
    });

    function graficar_event(){
        var chart = Highcharts.chart('container', {
            chart: {
                type: 'column'
            },

            title: {
                text: 'FACTOR DE HORA PICO'
            },

            subtitle: {
                text: ''
            },

            legend: {
                align: 'right',
                verticalAlign: 'middle',
                layout: 'vertical'
            },

            xAxis: {
                categories: app.categories_data_grapic,
                labels: {
                    x: -10
                }
            },

            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Totales'
                }
            },

            series: [{
                name: 'Cantidad',
                data: app.series_data_grapic,
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            }
        });
        $('.highcharts-credits').remove();
    }
  </script>
@endpush