@extends('layouts.admin.base')

@section('title', 'Indicadores')

@push('styles2')
  <link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
@endpush

@section('content')
<div id="vue">
	<div class="">
	    <div class="page-title">
	      <div style="text-align:center">
             <center><h2><b><a style="font-size:12px; margin-right:390px;"  href="{{ url('admin/indicadores/VPC') }}">Anterior Indicador</a><b><b>TWEETS<b><b><a style="font-size:12px; margin-left:380px;"  href="{{ url('admin/indicadores/VPT') }}">Siguiente Indicador >></a><b></h2><center>
            
	      </div>
	   </div>
   </div>
   <br />
   <br />
         <label style="float:left; font-size:20px;">Fecha Inicio:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_inicial" required>
       <label style="float:left; font-size:20px; margin-left:10px">Fecha Final:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_final" required>
	   <button @click ="btn_graficar()" class="btn btn-primary" style="margin-left:16px"><i class="fa fa-bar-chart"></i> Graficar</button>
    <br />
    <br />
  <div>
    <!-- <pre>@{{ $data }}</pre> -->
    <div class="table-md-responsive">
      <div id="container" class="highcharts-strong"></div>
    </div>
    <!-- <div id="container" style="min-width: 210px; height: 300px; margin: 0 auto"></div> -->
  </div>
</div>
  

@endsection

@push('script')
  <script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
  <script src="{{ asset('vendors/highgraphics/code/modules/timeline.js ') }}"></script>
  <script src="{{ asset('js/lodash.min.js') }}"></script>
	<script src="{{ asset('js/vue.js') }}"></script>
	<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
  <script>
    var app = new  Vue({
      el:"#vue",
      data:{
        positivos:[],
        negativos:[],
        series_data:[],
        drilldown_data:[],
        fecha_inicial:null,
        fecha_final:null,
      },
      created:function(){
        
      },
    methods:{
        graficar:function(){
            graficar_event();
        },
        btn_graficar(){
        if(_.isEmpty(app.fecha_inicial) || _.isEmpty(app.fecha_final)){
            Swal.fire('','Los campos de fechas no deben estar vacios','error');
        }else{
            $.post('/admin/indicadores/twitter',{
              fecha_inicial: app.fecha_inicial,
              fecha_final: app.fecha_final
            },function(response){
                if(!response.existeError){
                    app.positivos = _.filter(response.data, ['sentimiento', 'positivo']);
                    app.negativos = _.filter(response.data, ['sentimiento', 'negativo']);
                    //app.vehiculos_en_circulacion = response.data;
                    app.series_data = [];

                    $.each(response.data, function(i, item) {
                        app.series_data.push({
                            'name': item.created_at,
                            'label': item.userlocation + '<br />Tuits: ' + item.sentimiento,
                            'description': item.tuits
                            }
                        );
                    });
                    app.graficar();
                    sentimiento('Existe un total de '+_.size(app.negativos)+'Twitter NEGATIVOS, Mientras que POSITIVOS se alcanzo un total de '+_.size(app.positivos),'info');
                }else{
                    Swal.fire('','No existen Registros para mostrar','warning');
                }
            },'json');
        }
        },
      },
      watch:{

      }
    });

    function graficar_event(){
        Highcharts.chart('container', {
            chart: {
                type: 'timeline',
                scrollablePlotArea: {
                    minWidth: 20000,
                    scrollPositionX: 0
                }
            },
            xAxis: {
                visible: true
            },
            yAxis: {
                visible: true
            },
            title: {
                text: '<strong>ACCIDENTES Y/O EMBOTELLAMIENTO REPORTADOS</strong>'
            },
            subtitle: {
                text: ''
            },
            colors: [
                '#4185F3',
                '#427CDD',
                '#406AB2',
                '#3E5A8E',
                '#3B4A68',
                '#363C46',
                '#406AB2',
                '#363C46',
            ],
            series: [{
                data: app.series_data
            }]
        });
        $('.highcharts-credits').remove();
    }
  </script>
@endpush