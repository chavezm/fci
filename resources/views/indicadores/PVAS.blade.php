@extends('layouts.admin.base')

@section('title', 'Indicadores')

@push('styles2')
	<link href="{{asset('packages/css/indicador.css')}}" rel="stylesheet">
@endpush

@section('content')
<div id="vue">
	<div class="">
	    <div class="page-title">
	      <div style="text-align:center">
             <center><h2><b><a style="font-size:12px; margin-right:240px;"  href="{{ url('admin/indicadores') }}">Página Principal</a><b><b>PORCENTAJES DE VEHÍCULOS POR SECTOR<b><b><a style="font-size:12px; margin-left:210px;"  href="{{ url('admin/indicadores/VPC') }}">Siguiente Indicador >></a><b></h2><center>
            
	      </div>
	   </div>
   </div>
   <br />
   <br />
       <label style="float:left; font-size:20px;">Fecha Inicio:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_inicial" required>
       <label style="float:left; font-size:20px; margin-left:10px">Fecha Final:</label> <input type="date" class="form-control" style="width:165px; float:left; margin-left:8px" v-model="fecha_final" required>
	   <button @click ="graficarSector()" class="btn btn-primary" style="margin-left:16px"><i class="fa fa-bar-chart"></i> Graficar</button>
    <br />
    <br />
    <div class="row">
    	<div class="col-md-6 col-sm-12">
			<div id="mapa" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
		</div>
        <br />
    	<div class="col-md-6 col-sm-12">
			<!--<pre>@{{ $data }}</pre>-->
			<div id="container" style="min-width: 210px; height: 500px; margin: 0 auto"></div>
		</div>
    </div>
    
</div>
	

@endsection

@push('script')
	<script src="{{ asset('vendors/highgraphics/code/highcharts.js') }}"></script>
	<script src="{{ asset('vendors/highgraphics/code/graficas.js') }}"></script>
	<script src="{{ asset('vendors/highgraphics/code/modules/data.js') }}"></script>
	<script src="{{ asset('vendors/highgraphics/code/modules/drilldown.js') }}"></script>
	<script src="{{ asset('js/lodash.min.js') }}"></script>
	<script src="{{ asset('js/vue.js') }}"></script>
	<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
	<!-- mapa -->
	<script src="{{ asset('/estilos_sectores/leaflet-src.js') }}"></script>
	<script src="{{ asset('js/mapa.js') }}"></script>
	<script>
		var app = new  Vue({
			el:"#vue",
			data:{
				sectors:[],
				graficoTest:[],
				click:[],
				detalle:[],
				fecha_inicial:'',
				fecha_final:'',
			},
			created:function(){
				
			},
			methods:{
				graficar:function(){
					graficar_event();
					dibujar(app.sectors); // MAPA
				},
				getDetalle(name,id,idSector){
					$.post('/admin/indicadores/PVAS/detalle/'+idSector,{
						fecha_inicial: app.fecha_inicial,
						fecha_final: app.fecha_final
					},function(response){
						var detalle_ = [];
						$.each(response, function(i, item) {
							var a = [item.nombre,(item.porcentaje *1)];
							detalle_.push(a);
						});
						app.click.push({
					    	'name':name,
					    	'id':id,
					    	'data': detalle_
					    });
					},'json');
				},
				graficarSector:function(){
					if(_.isEmpty(app.fecha_inicial) || _.isEmpty(app.fecha_final)){
			            Swal.fire('','Los campos de fechas no deben estar vacios','error');
			        }else{
						$.post('/admin/indicadores/PVAS',{
							fecha_inicial: app.fecha_inicial,
							fecha_final: app.fecha_final
						},function(response){
							if(!response.existeError){
								app.sectors = response.data;
								app.graficoTest = [];
								$.each(response.data, function(i, item) {
								    app.graficoTest.push({
								    	'name'		: item.sector,
								    	'y'			: (item.porcentaje * 1),
								    	'drilldown' : item.sector
								    });
								    app.getDetalle(item.sector,item.sector,item.id_sector,item.conteo);
								});
								app.graficar();
								// VALIDACION DE RECOMENDACION
								var obj = _.maxBy(app.sectors, function(o) { return _.toInteger(o.porcentaje); });
								recomendacion('Debido que el '+ obj.sector + ' registra el mayor porcentaje ' + obj.porcentaje + '%, y que la mayor cantidad de vehículos son los de uso particular, se recomienda concientizar a las personas para que usen el auto particular de manera compartida, o a su vez hacer uso del transporte público ','info');
							}else{
								Swal.fire('','No existen Registros para mostrar !','warning');
							}
						},'json');
					}
				},
			},
			watch:{

			}
		});

	$('.highcharts-drillup-button').on('click',function(e){
		alert(e);
	});

	function graficar_event(){
		// Create the chart
		Highcharts.chart('container', {
		    chart: {
		    	events: {
			    	click:function(e){
			        		dibujar(app.sectors); // MAPA
			        	},
			            drilldown: function (e) {
			                if (!e.seriesOptions) {

			                    var chart = this,
			                        series = [];

			                        chart.showLoading('Cargando Vehículos...');

			                        var encontrado = [];
			                        $.each(app.sectors, function(i, item) {
									    if(item.sector == e.point.name){
									    	encontrado = item;
									    }
									});

			                        $.post('/admin/indicadores/PVAS/detalle/'+encontrado.id_sector,{
										fecha_inicial: app.fecha_inicial,
										fecha_final: app.fecha_final
									},function(response){
										var detalle_ = [];
										$.each(response, function(i, item) {
											var a = [item.nombre,(item.porcentaje *1)];
											detalle_.push(a);
										});
										series.push({
									    	'name':encontrado.sector,
									    	'id':encontrado.sector,
									    	'data': detalle_
									    });

									    chart.hideLoading();
			                        	chart.addSeriesAsDrilldown(e.point, series[0]);

			                        	//dibujar(response); // MAPA
									},'json');
			                }
			            }
				},
		        type: 'column'
		    },
		    title: {
		        text: 'PORCENTAJES DE VEHÍCULOS POR SECTOR'
		    },
		    subtitle: {
		        text: '<b>Desde: ' + app.fecha_inicial + '  <br><b>Hasta: ' + app.fecha_final 
		    },
		    xAxis: {
		        type: 'category'
		    },
		    yAxis: {
		        title: {
		            text: 'PORCENTAJES'
		        }

		    },
		    legend: {
		        enabled: false
		    },
		    plotOptions: {
		        series: {
		            borderWidth: 0,
		            dataLabels: {
		                enabled: true,
		                format: '{point.y:.2f}%'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		    },

		    series: [
		        {
		            name: "Datos",
		            colorByPoint: true,
		            data: app.graficoTest
		        }
		    ],
		    drilldown: {
		        series: []//app.click
		    }
		});
		$('.highcharts-credits').remove();
		}
	</script>
@endpush