<?php 

Route::get('admin/indicadores', 'IndicadoresController@Indicadores');


	Route::prefix('admin/indicadores')->group(function(){
		Route::namespace('Indicadores')->group(function(){
			// -----------------------------------------------------
			// PORCENTAJE DE VEHICULOS POR SECTOR
			// -----------------------------------------------------
			// Sectores
			Route::post('PVAS', 'VehiculoSectorController@IndicadoresSectores');
			// Detalle de sectores
			Route::post('PVAS/detalle/{id_sector?}', 'VehiculoSectorController@IndicadoresDetalle');

			// -----------------------------------------------------
			// VEHICULOS PRIVADOS EN CIRCULACION
			// -----------------------------------------------------
			Route::post('VPC', 'VehiculoCirculacionController@VehiculosEnCirculacion');
			// Detalle de Vehiculos privados en curculacion
			Route::post('VPC/detalle/{anio?}', 'VehiculoCirculacionController@IndicadoresDetalle');

			// -----------------------------------------------------
			// ACCIDENTES Y/O EMBOTELLAMIENTO REPORTADOS
			// -----------------------------------------------------
			Route::post('twitter', 'EmbotellamientoController@IndicadoresTwitter');

			// -----------------------------------------------------
			// FACTOR DE HORA PICO
			// -----------------------------------------------------
			Route::post('FHP', 'HoraPicoController@IndicadoresFhp');
			Route::post('FHPSector', 'HoraPicoController@listarSector');

			// -----------------------------------------------------
			// VELOCIDAD PROMEDIO DE LOS TIPOS DE VEHICULOS P0R SECTOR
			// -----------------------------------------------------
			Route::post('VPT', 'VelocidadPromedioController@IndicadoresVpt');
			Route::post('VPT-tipo-vehiculo', 'VelocidadPromedioController@listarTipoVehiculo');

		});
		// INDEX Porcentaje de vehiculos por sector
		Route::get('PVAS', 'IndicadoresController@IndicadoresPVAS');
		// INDEX Vehiculos privados en circulacion
		Route::get('VPC', 'IndicadoresController@IndicadoresVPC');
		// INDEX ACCIDENTES Y/O EMBOTELLAMIENTO REPORTADOS
		Route::get('twitter', 'IndicadoresController@IndicadoresTwitter');
		// INDEX FACTOR DE HORA PICO
		Route::get('FHP', 'IndicadoresController@IndicadoresFHP');
		// INDEX VELOCIDAD PROMEDIO DE LOS TIPOS DE VEHICULOS P0R SECTOR
		Route::get('VPT','IndicadoresController@IndicadoresVPT');
	});

?>