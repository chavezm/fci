library(DBI)
library(RPostgres)

args    <- commandArgs(TRUE)
#parametros
usuario <- args[1]
cluster <- args[2]
uidsector <- args[3]
#Se define la ruta en la qual se va a almanecar o eliminar en caso de que ya exista la imagen PNG que muestra el resultado KMEANS por cada sector...

#RUTA PARA LINUX
#SE ELIMINA LA CARPETA KMEANS PORQUE NO SE COPIO AL MOMENTO DE HACER EL PASE A PRODUCCION...
ruta1<-"/var/www/fci/public/img/images/"
#RUTA PARA WINDOWS
#ruta1<-"C:\\xampp\\htdocs\\fci\\public\\img\\images\\KMEANS\\"
ruta2<-paste(ruta1,uidsector, sep="")
ext<-".png"
ruta3<-paste(ruta2,ext, sep="")
#Con el comando unlink procedemos a borrar las imagenes PNG de analisis anteriores....
unlink(ruta3, recursive = FALSE, force = TRUE)
#conexion a la base
#conn=dbConnect(RPostgres :: Postgres (),host="52.38.27.79",port="5432",dbname="gye_datoss",user="Administrador",password="admin1234")
conn=dbConnect(RPostgres :: Postgres (),host="54.203.150.245",port="5432",dbname="datos_gye",user="SECTORESD",password="SECTORESD2019")
#conn=dbConnect(RPostgres :: Postgres (),host="localhost",port="5434",dbname="FCI_LOCAL",user="postgres",password="12345678")
#Concatenar usuario
query<-"SELECT longitud, latitud FROM public.trayectoria_gye_hist where usuario ='"
query_two<-paste(query,usuario, sep="")
query_tree<-"'"
queryfor<-paste(query_two,query_tree, sep="")
queryfive<-" and uidsector ="
querysix<-paste(queryfor,queryfive, sep="")
queryseven<-paste(querysix,uidsector, sep="")
#extraer los datos
datos_query=dbGetQuery(conn,queryseven)
#agrupar los datos
datos<-rbind(datos_query)
#generar analisis kmeans
kmeans.res<-kmeans(datos,center=cluster)
#guardar imagen
png(filename=ruta3, width = 800, height = 600)
#realiza el grafico
plot(datos,col=kmeans.res$cluster)
#pinta el cluster
points(kmeans.res$centers,cex=2,col=11,pch=19)


dev.off()
test<- kmeans.res[2]
puntos<- test[["centers"]]
#print(puntos)
latitudes<-puntos[,"latitud"]
longitudes<-puntos[,"longitud"]
print (latitudes)
print (longitudes)










