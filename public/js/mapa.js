
let map = null;
let mmr = null;

function dibujar(data){
	if(map) {
		map.remove();
	}
	
	map = new L.Map('mapa').setView([-2.168784,-79.927425], 11);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		minZoom: 5,	maxZoom: 19,
	    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	    id: 'mapbox.satellite'
	}).addTo(map);

    mmr = L.marker([0,0]);
	mmr.bindPopup('0,0',{autoClose:false});
	mmr.addTo(map);

    //Desactivar Zoom con el Mouse
    map.scrollWheelZoom.disable();

  // Nuevo
  dibujoMasivo(data);
}

  function dibujoMasivo(data){
  	$.each(data, function(i, item) {
  		var coordenadas = new L.geoJson(JSON.parse(item.coordenadas)).addTo(map); 

  		var puntomedio = new L.geoJson(JSON.parse(item.puntomedio)).addTo(map);

        /*$.each(JSON.parse(item.coordenadas).geometry.coordinates,function(index,item2){
        	$.each(item2,function(ii,coordenadass){
				let latlng = '{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":['+coordenadass[0]+','+coordenadass[1]+']}}';
				new L.geoJson(JSON.parse(latlng)).addTo(map);
        	});
		});*/

		let name = '';
        if(item.porcentaje){
        	if(item.nombre){
        		name = item.nombre + ' <strong>' + _.floor(item.porcentaje,2) + ' % </strong> </br>' + item.conteo + ' Vehículos';
        	}else{
        		name = '<strong> Sector: </strong>' + item.sector + '</br> <strong>' + _.floor(item.porcentaje,2) + ' % </strong> = ' + item.total + ' Vehículos';
        	}
        }else if(item.velocidad){ name = ' <strong> ' + item.sector + ' </strong> </br> ' + item.nombre + ' <strong> ' + _.floor(item.velocidad,2) + ' km/h </strong>';
        }else if(item.vehiculos_privados){ name = '<strong> Año: ' + item.año + '<strong><br/>SECTOR: </strong>' + item.sector + ' </br>'  + _.floor(item.vehiculos_privados,2) + ' veh </strong>';
        }else{ name = item.sector; }
        coordenadas.bindPopup(name).openPopup();
        coordenadas.bindPopup(name,{autoClose:false}).openPopup();
	});
	$('div[class="leaflet-control-attribution leaflet-control"]')
      	.append(" ");
}

function dibujoMasivo2(data){
	data.forEach(function(item,index){
		L.marker([item.lat, item.lng]).addTo(map)
		.bindPopup("<b class='text-danger'>" + item.nombre + '</b>',{autoClose:false}).openPopup();
	});
}